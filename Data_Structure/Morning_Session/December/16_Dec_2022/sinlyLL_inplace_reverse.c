//Singly linked list inplace reverse
#include<stdio.h>
#include<stdlib.h>
typedef struct Demo{
	int data;
	struct Demo *next;
}Demo;
Demo *head=NULL;
Demo *createNode(){
	Demo *newNode=(Demo*)malloc(sizeof(Demo));
	printf("Enter data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;

}
void addNode(){
	Demo *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		Demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newNode;
	}

}
void reverse(){
	if(head==NULL){
		printf("original linked list is empty\n");
	}else{
	Demo *temp1=NULL;
	Demo *temp2=head;
	while(head!=NULL){
		temp2=head;
		head=head->next;
		temp2->next=temp1;
		temp1=temp2;
	}
	head=temp1;
	}
}
void printLL(){
	if(head==NULL){
                printf("linked list is empty\n");
        }else{
	Demo *temp=head;
	while(temp->next!=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);

	}
}
void main(){
	int node;
	printf("Enter node:\n");
	scanf("%d",&node);
	for(int i=1;i<=node;i++){
		addNode();
	}
	printf("Original Singly linked list\n");
	printLL();
	printf("Reverse singly linked list\n");
	reverse();
	printLL();
}
