//Doubly Linked list Inplace reverse
#include<stdio.h>
#include<stdlib.h>
struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
};
struct Node *head=NULL;
struct Node* createNode(){
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->prev=NULL;
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
void reverse(){
	if(head==NULL){
                printf("original linked list is empty\n");
        }else{
        struct Node *temp=NULL;
        while(head!=NULL){
                temp=head->prev;
                head->prev=head->next;
                head->next=temp;
                head=head->prev;
          }
	head=temp->prev;
	}
}
void printLL(){
	if(head==NULL){
		printf("Empty linked list\n");
	}else{
		struct Node *temp=head;
                while(temp->next!=NULL){
			printf("|%d|->",temp->data);
                        temp=temp->next;
                }
		printf("|%d|",temp->data);
	}
}

void main(){
	int nodeCount;
	printf("Enter node count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printf("Original Singly linked list\n");
        printLL();
        printf("\nReverse singly linked list\n");
        reverse();
        printLL();

}
