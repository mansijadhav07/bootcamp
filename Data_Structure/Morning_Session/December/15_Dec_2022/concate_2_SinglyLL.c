//Concate two singly linked list
#include<stdio.h>
#include<stdlib.h>
struct Node{
	int data;
	struct Node *next;
};
struct Node *head1=NULL;
struct Node *head2=NULL;

struct Node* createNode(){
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}

struct Node* addNode(struct Node *head){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
	return head;
}
void concateLL(){
	struct Node *temp=head1;
	while(temp->next!=NULL){
		temp=temp->next;
	}
	temp->next=head2;
}

void printLL(struct Node *head){
	if(head==NULL){
		printf("Empty Linked list\n");
	}else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|",temp->data);
        }
}
void main(){
	int nodeCount;
	printf("Enter node count:linked list 1\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		head1=addNode(head1);
	}
	printLL(head1);
	printf("\nEnter node count:linked list 2\n");
        scanf("%d",&nodeCount);

	for(int i=1;i<=nodeCount;i++){
                head2=addNode(head2);
        }
        printLL(head2);
	concateLL();
	printf("\nConcated linked list \n");
	printLL(head1);;
}


