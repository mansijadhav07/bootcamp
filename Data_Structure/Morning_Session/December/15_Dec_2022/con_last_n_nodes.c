//Concate last n Nodes in singly Linked List
#include<stdio.h>
#include<stdlib.h>
struct Node{
	int data;
	struct Node *next;
};
struct Node *head1=NULL;
struct Node *head2=NULL;

struct Node* createNode(){
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}

struct Node* addNode(struct Node *head){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
	return head;
}
int countNode(struct Node *head){
	int count=0;
	struct Node *temp=head;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	return count;
}
void concateNLL(int num){
	if(num<=0){
		printf("Invalid node count\n");
	}else if(num>countNode(head2)){
		printf("Excess No of node count\n");
	}else{
	struct Node *temp1=head1;
	while(temp1->next!=NULL){
		temp1=temp1->next;
	}
	int val;
	val=countNode(head2)-num;
	struct Node *temp2=head2;
	while(val){
		temp2=temp2->next;
		val--;
	}
	temp1->next=temp2;
	}
}

void printLL(struct Node *head){
	if(head==NULL){
		printf("Empty Linked list\n");
	}else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|",temp->data);
        }
}
void main(){
	int nodeCount;
	printf("Enter node count:linked list 1\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		head1=addNode(head1);
	}
	printLL(head1);
	printf("\nEnter node count:linked list 2\n");
        scanf("%d",&nodeCount);

	for(int i=1;i<=nodeCount;i++){
                head2=addNode(head2);
        }
        printLL(head2);
	int num;
	printf("\nEnter no of nodes to concate\n");
	scanf("%d",&num);
	concateNLL(num);
	printf("Concated linked list \n");
	printLL(head1);;
}


