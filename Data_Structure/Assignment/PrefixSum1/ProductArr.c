/*Product Array Puzzle
  Given an array of integers A.find and return the product array of the same size
  where the ith element of the product array will be equal to the product of all
  the elements divided by ith element of array
  
  INPUT:arr{1,2,3,4,5}
  OUTPUT:arr{120,60,40,30,24}
*/
#include<stdio.h>
void product_Arr(int *arr,int size){
	int product=1;

	for(int i=0;i<size;i++){
		product=product*arr[i];
	}
	for(int i=0;i<size;i++){
		arr[i]=product/arr[i];
	}
}
void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("Array elements:\n");
	for(int i=0;i<size;i++){
                printf("| %d |",arr[i]);
        }
	printf("\n");

	product_Arr(arr,size);

	printf("Product Array:\n");

	for(int i=0;i<size;i++){
                printf("| %d |",arr[i]);
        }
	printf("\n");
}
