/*In place prefix sum
  -Given an array A of N integers
  -Construct the prefix sum of the array in the given array itself
  -Return the array of integers denoting the prefix sum of given array*/
#include<stdio.h>
void prefixSum(int *arr,int size){
	for(int i=1;i<size;i++){
		arr[i]=arr[i-1]+arr[i];
	}
}
void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("Original Array\n");
	for(int i=0;i<size;i++){
                printf("%d ",arr[i]);
        }
        printf("\n");
	prefixSum(arr,size);

	printf("In place prefix sum array\n");
	for(int i=0;i<size;i++){
                printf("%d ",arr[i]);
        }
	printf("\n");
}
