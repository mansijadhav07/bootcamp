/* Equilibrium index of the array
   -You are given an array A of integers of size N
   -Your task is to find the equilibrium index of the given array
   -The equilibrium index of an array is an index such that the sum of elements
    at lower indexes are equal to the sum of elements at higher indexes
   -If there are no elements that are at lower indexes or at higher indexes,
    then the corresponding sum of elements is considered as 0
*/
#include<stdio.h>
int EquilibriumArr(int arr[],int size){
	int pSum[size];
	pSum[0]=arr[0];
	for(int i=1;i<size;i++){
		pSum[i]=pSum[i-1]+arr[i];
	}
	for(int j=1;j<size;j++){
		if(pSum[j-1]==pSum[size-1]-pSum[j]){
			return j;
		}
	}
	return -1;
}
		       
void main(){
	int size;
        printf("Enter size of an array\n");
        scanf("%d",&size);
        int arr[size];
        printf("Enter array elements\n");
        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("Array elements:\n");
        for(int i=0;i<size;i++){
                printf("| %d |",arr[i]);
        }

        printf("\n");

	int Index=EquilibriumArr(arr,size);
	printf("Equilibrium array index is %d\n",Index);
}
