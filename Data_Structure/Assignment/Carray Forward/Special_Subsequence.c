/*
 Special Subsequence "AG"
  
 Problem Description
 -You have a given string having uppercase letter
 -You have to find how many times the subsequence "AG" is their in the given string

 INPUT:"ABCGAG"
 OUTPUT:3
*/
#include<stdio.h>
int subsequence(char string[],int size){
	int count=0;
	for(int i=0;i<size;i++){
		if(string[i]=='A'){
			for(int j=i+1;j<size;j++){
				if(string[j]=='G'){
					count++;
				}
			}
		}
	}
	return count;
}
void main(){
	int size;
	printf("Enter size of string\n");
	scanf("%d",&size);
	char string[size];
         
	printf("Enter String\n");
	int i=0;
	char ch;
	getchar();
	while((ch=getchar())!='\n'){
		string[i++]=ch;
	}

	int ret=subsequence(string,size);

	printf("Count of subsequence=%d\n",ret);
}


