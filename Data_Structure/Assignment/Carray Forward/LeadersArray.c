/*
Leaders in array

Problem Description
-Given an integer array A containing N distinct intrgers,you have to find all the leaders in array A
-An element is a leader if it is strictly greater than all the elements to it's on the right side

INPUT
| 16 || 17 || 4 || 3 || 5 || 2 |
OUTPUT
| 2 || 5 || 17 |
*/

#include<stdio.h>
void LeadersArray(int arr[],int size){
	int RightMax[size];

	RightMax[size-1]=arr[size-1];

	int count=1;

	int Leader[size];

	Leader[0]=arr[size-1];
	for(int i=size-2;i>=0;i--){
	         if(RightMax[i+1]<arr[i]){
			 RightMax[i]=arr[i];
			 count++;
			 Leader[count-1]=arr[i];
		 }else{
			 RightMax[i]=RightMax[i+1];
		 }
	}
			       
	printf("Leaders Array\n");
        for(int i=0;i<count;i++){
                printf("| %d |",Leader[i]);
        }
	printf("\n");

}

void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);
        
	int arr[size];

	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Original Array\n");
        for(int i=0;i<size;i++){
                printf("| %d |",arr[i]);
        }

        printf("\n");

	LeadersArray(arr,size);
}
