//Singly Linked List
#include<stdio.h>
#include<stdlib.h>
struct Relay{
	char name[20];
	int distance;
	float record;
	struct Relay *next;
};

struct Relay *head=NULL;
struct Relay* createNode(){
	struct Relay *newNode=(struct Relay*)malloc(sizeof(struct Relay));
        getchar();	
	printf("Enter player name:\n");
	int i=0;
	char ch;
	while((ch=getchar())!='\n'){
		(*newNode).name[i]=ch;
		i++;
	}
	printf("Enter meter distance\n");
	scanf("%d",&newNode->distance);
	printf("Enter record of player (in sec)\n");
	scanf("%f",&newNode->record);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	struct Relay *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Relay *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void addFirst(){
	struct Relay *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		newNode->next=head;
		head=newNode;
	}
}
void addLast(){
       addNode();
}
int countNode(){
	if(head==NULL){
		printf("Linked List is empty\n");
	}else{
		int count=0;
		struct Relay *temp=head;
		while(temp!=NULL){
			count++;
			temp=temp->next;
		}
		return count;
	}
}


int addAtPos(int pos){
	int count=countNode();
	if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addLast();
		}else{
			struct Relay *newNode=createNode();
			struct Relay *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
		return 0;
	}
}
void deleteFirst(){
	int count=countNode();
	if(head==NULL){
		printf("Nothing To delete\n");
	
	}else if(count==1){
		free(head);
		head=NULL;
	}else{
		struct Relay *temp=head;
		head=temp->next;
		free(temp);
	}
}
void deleteLast(){
	int count=countNode();
	if(head==NULL){
		printf("Nothing to delete\n");
	}else if(count==1){
		free(head);
		head=NULL;
	}else{
		struct Relay *temp=head;
		while(temp->next->next!=NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}
int deleteAtPos(int pos){
	int count=countNode();
	if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return 0;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			struct Relay *temp1=head;
			struct Relay *temp2=head;
			while(pos-2){
                             temp1=temp1->next;
			     pos--;
			}
			temp2=temp1->next;
			temp1->next=temp2->next;
			free(temp2);
		}
	}
}

void printLL(){
	if(head==NULL){
		printf("Linked List is empty\n");
	}else{
		struct Relay *temp=head;
		while(temp->next!=NULL){
			printf("|%s",temp->name);
			printf(" %d",temp->distance);
			printf(" %f|->",temp->record);
                        temp=temp->next;
                 }
		printf("|%s",temp->name);
                printf(" %d",temp->distance);
                printf(" %f|",temp->record);
		printf("\n");

	}
}


void main(){
	char choice;
	do{
		printf("1. Add Node\n");
		printf("2. Add First\n");
		printf("3. Add At Position\n");
		printf("4. Delete First\n");
		printf("5. Delete Last\n");
		printf("6. Delete At Position\n");
		printf("7. Count Nodes\n");
		printf("8. Print Linked List\n");

		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
                                addFirst();
                                break;
			case 3:
				{
					int pos;
					printf("Enter the position:\n");
					scanf("%d",&pos);
					addAtPos(pos);
				}
				break;
			case 4:
				deleteFirst();
				break;
                        case 5: 
				deleteLast();
				break;
			case 6:{
				       int pos;
				       printf("Enter the position:\n");
				       scanf("%d",&pos);
				       deleteAtPos(pos);
			       }
			       break;
			case 7:
			       
			       printf("Count=%d\n",countNode());
			       
			       break;
			case 8:
			       printLL();
			       break;
			default:
			       printf("Wrong Choice\n");
		}getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice =='y' || choice =='Y');
}

