//Singly Circular Linked List
#include<stdio.h>
#include<stdlib.h>

typedef struct MonthlyCalender{
	char month[20];
	int days;
	float budget;
	struct MonthlyCalender *next;
}Calender;
Calender *head=NULL;
Calender* createNode(){
	Calender *newNode=(Calender*)malloc(sizeof(Calender));
	getchar();
	printf("Enter month:\n");
	int i=0;
	char ch;
	while((ch=getchar())!='\n'){
		(*newNode).month[i]=ch;
		i++;
	}
	printf("Enter days in month\n");
	scanf("%d",&newNode->days);
	printf("Enter monthly budget\n");
	scanf("%f",&newNode->budget);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	Calender *newNode=createNode();
	if(head==NULL){
		head=newNode;
		newNode->next=head;
	}else{
		Calender *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
	}
}
void addFirst(){
	Calender *newNode=createNode();
	if(head==NULL){
		head=newNode;
		newNode->next=head;

	}else{
		Calender *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
		head=newNode;
	}
}
void addLast(){
	addNode();
}
int countNode(){
	if(head==NULL){
		printf("Empty Linked List\n");
	}else{
		int count=0;
                Calender *temp=head;
		while(temp->next!=head){
			count++;
			temp=temp->next;
		}
		return count+1;
	}
}
int AddAtPos(int pos){
	int count=countNode();
	if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addLast();
		}else{
			Calender *newNode=createNode();
			Calender *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
		return 0;
	}
}
void deleteFirst(){
	if(head==NULL){
		printf("Nothing to delete\n");
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
		Calender *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		head=head->next;
		free(temp->next);
		temp->next=head;
		}
		
	}
}
void deleteLast(){
	if(head==NULL){
		printf("Nothing to delete\n");
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			Calender *temp=head;
			while(temp->next->next!=head){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=head;
		}
	}
}
int deleteAtPos(int pos){
	int count=countNode();
	if(head==NULL){
		printf("Nothing to delete\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			Calender *temp1=head;
			Calender *temp2=head;
			while(pos-2){
				temp1=temp1->next;
				pos--;
			}
			temp2=temp1->next;
			temp1->next=temp2->next;
			free(temp2);
		}
		return 0;
	}
}

void printLL(){
	if(head==NULL){
		printf("Empty Linked List\n");
	}else{
		Calender *temp=head;
		while(temp->next!=head){
			printf("|%s",temp->month);
			printf(" %d ",temp->days);
			printf("%f|->",temp->budget);
			temp=temp->next;
		}
		printf("|%s",temp->month);
                printf(" %d ",temp->days);
                printf("%f|",temp->budget);
		printf("\n");
	}
}
void main(){
	char choice;
	do{
		printf("1. Add Node\n");
		printf("2. Add First\n");
		printf("3. Add At Position\n");
		printf("4. Delete First\n");
		printf("5. Delete Last\n");
		printf("6. Delete At Position\n");
		printf("7. Count Nodes\n");
		printf("8. Print Linked List\n");

		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
                                addFirst();
                                break;
			case 3:
				{
					int pos;
					printf("Enter the position:\n");
					scanf("%d",&pos);
					AddAtPos(pos);
				}
				break;
			case 4:
				deleteFirst();
				break;
                        case 5:
				deleteLast();
				break;
			case 6:{
				       int pos;
				       printf("Enter the position:\n");
				       scanf("%d",&pos);
				       deleteAtPos(pos);
			       }
			       break;
                        case 7:

                               printf("Count=%d\n",countNode());

                               break;
                        case 8:
                               printLL();
                               break;
                        default:
                               printf("Wrong Choice\n");
                }getchar();
                printf("Do you want to continue?\n");
                scanf("%c",&choice);
        }while(choice =='y' || choice =='Y');
}

  

