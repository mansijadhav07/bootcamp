//Doubly Circular Linked List
#include<stdio.h>
#include<stdlib.h>
typedef struct Ring_Topology{
	struct Ring_Topology *prev;
	char Device[10];
	float ipv4;
	struct Ring_Topology *next;
}ring;
ring *head=NULL;
ring* createNode(){
	ring *newNode=(ring*)malloc(sizeof(ring));
	getchar();
	newNode->prev=NULL;
	printf("Enter Device name:\n");
	int i=0;
	char ch;
	while((ch=getchar())!='\n'){
		(*newNode).Device[i]=ch;
		i++;
	}
	printf("Enter ipv4 address of device\n");
	scanf("%f",&newNode->ipv4);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	ring *newNode=createNode();
	if(head==NULL){
		head=newNode;
		head->prev=head;
		newNode->next=head;
	}else{
		newNode->next=head;
		head->prev->next=newNode;
		newNode->prev=head->prev;
		head->prev=newNode;
	}
}
void addFirst(){
	ring *newNode=createNode();
	if(head==NULL){
		head=newNode;
		newNode->next=head;
		head->prev=newNode;
	}else{
		newNode->prev=head->prev;
		head->prev->next=newNode;
		newNode->next=head;
		head->prev=newNode;
		head=newNode;
	}
}
void addLast(){
	addNode();
}
int countNode(){
	if(head==NULL){
		printf("Empty Linked List\n");
	}else{
		int count=0;
		ring *temp=head;
		while(temp->next !=head){
			count++;
			temp=temp->next;
		}
		return count+1;
		printf("%d count",count);
	}
}
int addAtPos(int pos){
	int count=countNode();
	if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addLast();
		}else{
			ring *newNode=createNode();
			ring *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			newNode->next->prev=newNode;
			temp->next=newNode;
			newNode->prev=temp;
		}
		return 0;
	}
}
void deleteFirst(){
	if(head==NULL){
		printf("Nothing to delete\n");
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			head=head->next;
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
		}
	}
}
void deleteLast(){
	if(head==NULL){
		printf("Nothing to delete\n");
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
		}
	}
}
int deleteAtPos(int pos){
	int count=countNode();
	if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			ring *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
		return 0;
	}
}


void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
		ring *temp=head;
		while(temp->next!=head){
			printf("|%s ",temp->Device);
			printf("%f|->",temp->ipv4);
			temp=temp->next;
		}
		printf("|%s ",temp->Device);
                printf("%f|",temp->ipv4);
		printf("\n");
	}
}
void main(){
	char choice;
	do{
		printf("1. Add Node\n");
		printf("2. Add First\n");
		printf("3. Add At Position\n");
		printf("4. Delete First\n");
		printf("5. Delete Last\n");
		printf("6. Delete At Position\n");
		printf("7. Count Nodes\n");
		printf("8. Print Linked List\n");

		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
                                addFirst();
                                break;
			case 3:
				{
					int pos;
					printf("Enter the position:\n");
					scanf("%d",&pos);
					addAtPos(pos);
				}
				break;
			case 4:
				deleteFirst();
				break;
                        case 5:
				deleteLast();
				break;
			case 6:{
				       int pos;
				       printf("Enter the position:\n");
				       scanf("%d",&pos);
				       deleteAtPos(pos);
			       }
			       break;
		        case 7:
                               printf("Count=%d\n",countNode());
                               break;
                        case 8:
                               printLL();
                               break;
                        default:
                               printf("Wrong Choice\n");
                }getchar();
                printf("Do you want to continue?\n");
                scanf("%c",&choice);
        }while(choice =='y' || choice =='Y');
}





