//count of array element having atleast one element greater than itself
//Optimized Approach 1
#include<stdio.h>
int countNo(int *arr,int size){
	int maxCount=0;
	int max=arr[0];
	for(int i=0;i<size;i++){
		if(arr[i]>max){
			max=arr[i];
		}
	}
	for(int i=0;i<size;i++){
		if(max==arr[i]){
			maxCount++;
		}
	}
	int count=size-maxCount;
	return count;
}
void main(){
	int arr[]={7,3,-2,1,4,5,1,4};
	int size=sizeof(arr)/sizeof(arr[0]);
	int count=countNo(arr,size);
	printf("Count=%d\n",count);
}
