//count of array element having atleast one element greater than itself
//Brute force approach
#include<stdio.h>
int countNo(int *arr,int size){
	int count=0;
	for(int i=0;i<size;i++){
		int num=arr[i];
		for(int j=0;j<size;j++){
			if(arr[j]>num){
				count++;
				break;
			}
		}
	}
	return count;
}
void main(){
	int arr[]={7,3,-2,1,4,5,1,4};
	int size=sizeof(arr)/sizeof(arr[0]);
	int count=countNo(arr,size);
	printf("Count=%d\n",count);
}
