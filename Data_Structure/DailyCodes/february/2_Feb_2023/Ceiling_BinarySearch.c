//Find Ceiling value by Binary Search
#include<stdio.h>
int CeilingVal(int *arr,int size,int key){
	int start=0;
	int end=size-1;
	int mid;
	int store=-1;
	while(start<=end){
		int mid=(start+end)/2;
		if(arr[mid]==key){
			return arr[mid];
		}
		if(arr[mid]<key){
			start=mid+1;
		}
		if(arr[mid]>key){
			end=mid-1;
			store=arr[mid];
		}
	}	
	return store;
}
void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int key;
	printf("Enter key value\n");
	scanf("%d",&key);
	int ret=CeilingVal(arr,size,key);
	if(ret==-1){
		printf("There is no ceiling element for %d\n",key);
	}else{
        	printf("Ceiling value of %d is %d\n",key,ret);
	}
}
