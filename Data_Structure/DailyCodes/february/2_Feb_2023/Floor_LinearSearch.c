//Find floor value by Linear search
#include<stdio.h>
int floorVal(int *arr,int size,int key){
	for(int i=0;i<size;i++){
		if(arr[i]==key){
			return arr[i];
		}
	}
	for(int i=0;i<size;i++){
		if(arr[i]>key){
			return arr[i-1];
		}
	}
	return -1;
}
void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int key;
	printf("Enter key value\n");
	scanf("%d",&key);
	int ret=floorVal(arr,size,key);
	if(ret==-1){
		printf("There is no element is greater than %d\n",key);
	}else{
        	printf("Floor value of %d is %d\n",key,ret);
	}
}
