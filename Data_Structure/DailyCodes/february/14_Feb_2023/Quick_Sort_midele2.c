#include<stdio.h>
void printArr(int arr[],int size){
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
}
void swap(int *ptr1,int *ptr2){
	int temp=*ptr1;
	*ptr1=*ptr2;
	*ptr2=temp;
}
int partition(int arr[],int start,int end){
	int pivot=arr[end];
	int itr=start-1;
	for(int i=start;i<end;i++){
		if(arr[i]<pivot){
			itr++;
			swap(&arr[i],&arr[itr]);
		}
	}
	swap(&arr[itr+1],&arr[end]);
	return (itr+1);
}
void quickSort(int arr[],int start,int end){
	if(start<end){
		int pivot=partition(arr,start,end);
		quickSort(arr,start,pivot-1);
		quickSort(arr,pivot+1,end);
	}
}

void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("Array before sorting\n");
	printArr(arr,size);
	printf("\n");
	int end=size-1;
	quickSort(arr,0,end);
	printf("Array after sorting\n");
	printArr(arr,size);
	int mid=0+end/2;
	printf("\nMiddle element of sorted array is %d\n",arr[mid]);
}
	
