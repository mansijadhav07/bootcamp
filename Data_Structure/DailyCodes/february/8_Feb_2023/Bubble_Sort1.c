//Bubble Sort
#include<stdio.h>
void bubbleSort(int *arr,int size){
	for(int i=0;i<size;i++){
		for(int j=0;j<size-i-1;j++){
			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}
	}
}
void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Array before sorting\n");

	for(int i=0;i<size;i++){
		printf("| %d |",arr[i]);
	}
	printf("\n");

	bubbleSort(arr,size);

	printf("Array after sorting\n");

        for(int i=0;i<size;i++){
                printf("| %d |",arr[i]);
        }
        printf("\n");
}

