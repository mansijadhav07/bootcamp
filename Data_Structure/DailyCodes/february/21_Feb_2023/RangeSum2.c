#include<stdio.h>

void RangeSum(int pSum[],int size){
	int query;
	int start,end;
	printf("Enter query\n");
	scanf("%d",&query);
	for(int i=1;i<=query;i++){
		printf("Enter start\n");
		scanf("%d",&start);
		printf("Enter end\n");
		scanf("%d",&end);
		if(start<0 || end>size){
			printf("Invalid Input\n");
		}else{
			if(start<end){
			     if(start==0){
			        	printf("Sum of range index %d and %d is %d\n",start,end,pSum[end]);
		             }else if(start==end){
			        	printf("Sum of range index %d and %d is %d\n",start,end,pSum[end]);
		             }else{
			        	printf("Sum of range index %d and %d is %d\n",start,end,pSum[end]-pSum[start-1]);
			     }
			}else{
				printf("Start should be less than end\n");
			}
		}
	}
}
	
void main(){
        int arr[]={7,-3,2,1,4,5,1,4};
        int size=sizeof(arr)/sizeof(arr[0]);
	int pSum[size];
	pSum[0]=arr[0];
	for(int i=1;i<size;i++){
		pSum[i]=pSum[i-1]+arr[i];
	}
        RangeSum(pSum,size);
}

