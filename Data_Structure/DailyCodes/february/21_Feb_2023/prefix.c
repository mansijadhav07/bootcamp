//Prefix Sum Array
#include<stdio.h>
void prefixArr(int arr[],int size){
	int pSum[size];
	pSum[0]=arr[0];
	for(int i=1;i<size;i++){
		pSum[i]=pSum[i-1]+arr[i];
	}

	printf("Prefix Sum array\n");
	for(int i=0;i<size;i++){
		printf("%d ",pSum[i]);
	}
	printf("\n");
}
void main(){
	int arr[]={3,4,4,5,5,49,30};
	int size=sizeof(arr)/sizeof(arr[0]);

	printf("Original Array\n");
	for(int i=0;i<size;i++){
                printf("%d ",arr[i]);
        
	}
	printf("\n");

	prefixArr(arr,size);
}
