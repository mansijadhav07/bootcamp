//Brute Force approach
#include<stdio.h>
void RangeSum(int *arr,int size){
	int start,end;
	int query;
	printf("Enter number of query\n");
	scanf("%d",&query);
	for(int q=1;q<=query;q++){
		printf("Enter start\n");
		scanf("%d",&start);
		printf("Enter end\n");
                scanf("%d",&end);
		int sum=0;

		if(start<end){
		    for(int i=start;i<=end;i++){
		        	sum=sum+arr[i];
		    }
		    printf("Sum of array element from index %d to %d is %d\n",start,end,sum);
		}else{
			printf("Start should be less than end\n");
		}
	}
}
void main(){
	int arr[]={7,-3,2,1,4,5,1,4};
	int size=sizeof(arr)/sizeof(arr[0]);
	RangeSum(arr,size);
}
