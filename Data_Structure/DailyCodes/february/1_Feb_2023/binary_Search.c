//Binary Search
#include<stdio.h>
int binarySearch(int arr[],int size,int key){
	int start=0;
	int end=size-1;
	int mid;
	while(start<=end){
		mid=(start+end)/2;

		if(arr[mid]==key){
			return mid;
		}
		if(arr[mid]>key){
			end=mid-1;
		}
		if(arr[mid]<key){
			start=mid+1;
		}
	}
	return -1;
}
void main(){
	int size,key;
	printf("Enter size of array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array element\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	
	printf("Enter search element\n");
	scanf("%d",&key);
	int ret=binarySearch(arr,size,key);
	if(ret==-1){
		printf("key is not present in array\n");
	}else{
		printf("%d is present at %d index\n",key,ret);
	}
}
