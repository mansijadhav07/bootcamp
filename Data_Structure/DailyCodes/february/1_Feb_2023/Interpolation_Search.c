/*
Interpolation Search
Time Complexity: O(log2(log2 n)) for the average case, and O(n) for the worst case
Auxiliary Space Complexity: O(1)
-values in a sorted array are uniformly distributed
*/
#include<stdio.h>
int InterpolationSearch(int arr[],int size,int key){
        int start=0;
        int end=size-1;
        
        while(start<=end){
		int mid=start+(end-start)*((key-arr[start])/(arr[end]-arr[start]));
                if(arr[mid]==key){
                        return mid;
                }
                if(arr[mid]>key){
                        end=mid-1;
                }
                if(arr[mid]<key){
                        start=mid+1;
                }
        }
        return -1;
}
void main(){
        int size,key;
        printf("Enter size of array\n");
        scanf("%d",&size);
        int arr[size];
        printf("Enter array element\n");
        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }

        printf("Enter search element\n");
        scanf("%d",&key);
        int ret=InterpolationSearch(arr,size,key);
	if(ret==-1){
		printf("Element is not present in an array\n")
			;
	}else{
                printf("%d is present at %d index\n",key,ret);
	}
}

