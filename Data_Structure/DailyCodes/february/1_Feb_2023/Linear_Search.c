//Linear Search
#include<stdio.h>
int ispresent(int *arr,int size,int key){
	for(int i=0;i<size;i++){
		if(arr[i]==key){
			return i;
		}
	}
	return -1;
}
int lastOcc(int arr[],int size,int key){
	int LastOcc=-1;
	for(int i=0;i<size;i++){
                if(arr[i]==key){
                        LastOcc=i;
                }
        }
	return LastOcc;
}
int secondLastOcc(int arr[],int size,int key){
	int first=-1;
	int second=-1;
	for(int i=0;i<size;i++){
                if(arr[i]==key){
			second=first;
			first=i;
                }
        }
	return second;
}
void main(){
	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int key;
	printf("Enter element to be searched\n");
	scanf("%d",&key);

	int ret=ispresent(arr,size-1,key);
	if(ret==-1){
	     printf("%d is not present is array\n",key);
	}else{
	      printf("%d is present at %d index\n",key,ret);
	}
	int last;
	int secondlast;
	if(ret !=-1){
	     last=lastOcc(arr,size,key);
	     printf("Last occurnace of %d is %d\n",key,last);
	     secondlast=secondLastOcc(arr,size,key);
	     printf("Second Last occurance %d is %d\n",key,secondlast);
	 }

}
