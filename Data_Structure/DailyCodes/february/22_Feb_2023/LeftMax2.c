/*Carray forward Array
  Leftmax array
  Otimize code approach
  Time Complexity=O(N)
  Space Complexity=O(N)
*/
#include<stdio.h>
void LeftMaxArr(int *arr,int size,int *LeftMax){
	LeftMax[0]=arr[0];
	for(int i=1;i<size;i++){
		if(LeftMax[i-1]<arr[i]){
			LeftMax[i]=arr[i];
		}else{
			LeftMax[i]=LeftMax[i-1];
		}
	}
}
void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);
        
	int arr[size];

	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Original Array\n");
        for(int i=0;i<size;i++){
                printf("| %d |",arr[i]);
        }

        printf("\n");


	int LeftMax[size];

	LeftMaxArr(arr,size,LeftMax);

	printf("LeftMax Array\n");
        for(int i=0;i<size;i++){
                printf("| %d |",LeftMax[i]);
        }

	printf("\n");
}
/*
 OUTPUT
 Original Array
| 5 || 2 || 1 || -4 || -3 || 9 || 3 || 4 |
LeftMax Array
| 5 || 5 || 5 || 5 || 5 || 9 || 9 || 9 |
*/
