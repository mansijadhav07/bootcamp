/*Carray forward Array
  Leftmax array
  Brtue force approach
  Time Complexity=O(N^2)
  Space Complexity=O(N)
*/
#include<stdio.h>
void LeftMaxArr(int *arr,int size,int *LeftMax){
	for(int i=0;i<size;i++){
		int LMax=arr[i];
		for(int j=0;j<=i;j++){
			if(LMax<arr[j]){
				LMax=arr[j];
			}
		}
		LeftMax[i]=LMax;
	}
}
void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);
        
	int arr[size];

	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Original Array\n");
        for(int i=0;i<size;i++){
                printf("| %d |",arr[i]);
        }

        printf("\n");


	int LeftMax[size];

	LeftMaxArr(arr,size,LeftMax);

	printf("LeftMax Array\n");
        for(int i=0;i<size;i++){
                printf("| %d |",LeftMax[i]);
        }

	printf("\n");
}

