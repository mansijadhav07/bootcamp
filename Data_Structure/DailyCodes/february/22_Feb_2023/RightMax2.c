/*Carray forward Array
  RightMax array
  Otimize code approach
  Time Complexity=O(N)
  Space Complexity=O(N)
*/
#include<stdio.h>
void RightMaxArr(int *arr,int size,int *RightMax){
	RightMax[size-1]=arr[size-1];
	for(int i=size-2;i>=0;i--){
		if(RightMax[i+1]<arr[i]){
			RightMax[i]=arr[i];
		}else{
			RightMax[i]=RightMax[i+1];
		}
	}
}
void main(){
	int size;
	printf("Enter size of an array\n");
	scanf("%d",&size);
        
	int arr[size];

	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Original Array\n");
        for(int i=0;i<size;i++){
                printf("| %d |",arr[i]);
        }

        printf("\n");


	int RightMax[size];

	RightMaxArr(arr,size,RightMax);

	printf("RightMax Array\n");
        for(int i=0;i<size;i++){
                printf("| %d |",RightMax[i]);
        }

	printf("\n");
}
/* OUTPUT
Original Array
| 5 || 2 || 1 || -4 || -2 || 9 || 3 || 4 |
RightMax Array
| 9 || 9 || 9 || 9 || 9 || 9 || 4 || 4 |
*/
