//Counting Sort-negative values
#include<stdio.h>
void CountingSort(int arr[],int size){
	int min=arr[0];
	for(int i=1;i<size;i++){
		if(min>arr[i]){
			min=arr[i];
		}
	}
	if(min<0){
		min=-min;
	}
	for(int i=0;i<size;i++){
		arr[i]=arr[i]+min;
	}
	printf("Positive value array\n");
	for(int i=0;i<size;i++){
               printf("%d ",arr[i]);
        }
	int max=arr[0];
	for(int i=1;i<size;i++){
		if(max<arr[i]){
			max=arr[i];
		}
	}

	int countArr[max+1];

	for(int i=0;i<=max;i++){
		countArr[i]=0;
	}

	for(int i=0;i<size;i++){
		countArr[arr[i]]=countArr[arr[i]]+1;
	}
	
	for(int i=1;i<=max;i++){
		countArr[i]=countArr[i]+countArr[i-1];
	}
	int output[size];
	for(int i=size-1;i>=0;i--){
		output[countArr[arr[i]]-1]=arr[i];
		countArr[arr[i]]--;
	}
	
	for(int i=0;i<size;i++){
                output[i]=output[i]-min;
        }
	printf("\nSorted array\n");
	for(int i=0;i<size;i++){
		printf("%d ",output[i]);
	}
}
void main(){
        int arr[]={-3,7,-2,3,-4};
        int size=sizeof(arr)/sizeof(arr[0]);
        printf("Before sorting\n");
        for(int i=0;i<size;i++){
                printf("%d ",arr[i]);
        }
	printf("\n");
	CountingSort(arr,size);
}
	
