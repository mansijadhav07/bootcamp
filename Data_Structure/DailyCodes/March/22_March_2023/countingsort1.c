//Counting Sort-Positive values
#include<stdio.h>
void main(){
	int arr[]={3,7,2,1,8,2,5,2,7};
	int size=sizeof(arr)/sizeof(arr[0]);
        printf("Before sorting\n");
	for(int i=0;i<=size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
	int max=arr[0];
	for(int i=1;i<size;i++){
		if(max<arr[i]){
			max=arr[i];
		}
	}

	int countArr[max+1];

	for(int i=0;i<=max;i++){
		countArr[i]=0;
	}

	for(int i=0;i<=max;i++){
		countArr[arr[i]]=countArr[arr[i]]+1;
	}
	printf("counting array\n");
	for(int i=0;i<=max;i++){
		printf("%d ",countArr[i]);
	}
	printf("\n");

	for(int i=1;i<=max;i++){
		countArr[i]=countArr[i]+countArr[i-1];
	}
	int output[size];
	for(int i=size-1;i>=0;i--){
		output[countArr[arr[i]]-1]=arr[i];
		countArr[arr[i]]--;
	}
	printf("Sorted array\n");
	for(int i=0;i<size;i++){
                printf("%d ",output[i]);
        }
}
