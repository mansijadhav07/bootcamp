#include<stdio.h>
#include<stdlib.h>
typedef struct WebSeries{
	int season;
	float rating;
	struct WebSeries *next;
}web;
web *head=NULL;
void addNode(){
	web *newNode=(web*)malloc(sizeof(web));
	newNode->season=1;
	newNode->rating=8.3;
	newNode->next=NULL;
	head=newNode;
}
void printLL(){
	web *temp=head;
	while(temp!=NULL){
		printf("%d ",temp->season);
		printf("%f ",temp->rating);
		temp=temp->next;
	}
}
void main(){
	addNode();
	printLL();
}
