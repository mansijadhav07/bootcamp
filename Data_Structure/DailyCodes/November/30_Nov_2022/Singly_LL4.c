#include<stdio.h>
#include<stdlib.h>
typedef struct Student{
	int id;
	float ht;
	struct Student *next;
}stud;
stud *head=NULL;
void addNode(){
	stud *newNode=(stud*)malloc(sizeof(stud));
	newNode->id=1;
	newNode->ht=4.3;
	newNode->next=NULL;
	head=newNode;
}
void printLL(){
	stud *temp=head;
	while(temp!=NULL){
		printf("id=%d\n",temp->id);
		printf("ht=%f\n",temp->ht);
		temp=temp->next;
	}
}
void main(){
	addNode();
	printLL();
}
