#include<stdio.h>
#include<stdlib.h>
typedef struct Student{
	int id;
	float ht;
	struct Student *next;
}stud;
void main(){
	stud *head=NULL;
        //First Node
	stud *newNode=(stud*)malloc(sizeof(stud));
	newNode->id=1;
	newNode->ht=4.2;
	newNode->next=NULL;

	//Connect First Node
	head=newNode;

	//Second Node
	newNode=(stud*)malloc(sizeof(stud));
	newNode->id=2;
	newNode->ht=5.1;
	newNode->next=NULL;

	//Connect Second node to head
	head->next=newNode;

	//Third Node
	newNode=(stud*)malloc(sizeof(stud));
	newNode->id=3;
	newNode->ht=6.2;
	newNode->next=NULL;

	//Connect Third node to head
	head->next->next=newNode;

	stud *temp=head;
	while(temp!=NULL){
		printf("%d: ",temp->id);
		printf("%f ",temp->ht);
		temp=temp->next;
		printf("\n");
	}
	
}

