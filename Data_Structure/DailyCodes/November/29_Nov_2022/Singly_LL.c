#include<stdio.h>
#include<stdlib.h>
struct Node{
	int data;
	struct Node *next;
};
void main(){
	struct Node *head=NULL;
	//first node
	struct Node  *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=10;
	newNode->next=NULL;

	//Connecting first node to head
	head=newNode;

	//second node
	newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=20;
	newNode->next=NULL;

	//Connecting second node to head
        head->next=newNode;

	//Third Node
	newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=30;
	newNode->next=NULL;

	//connecting third node to head
	head->next->next=newNode;
        
	//temp->next !=NULL it has problem it cannot print last node because at 
	//last node temp->next=NULL condition is match so it can't print
	struct Node *temp=head;
	while(temp->next !=NULL){
		printf("%d ",temp->data);
		temp=temp->next;
         }
	printf("%d ",temp->data);//for printing last node because temp at last node
}

