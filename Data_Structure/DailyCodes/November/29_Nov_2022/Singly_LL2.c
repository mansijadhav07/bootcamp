//It is not a proper way to create singly linked list because if we want to print/create more node
//we have to type head->next->next->next.... it create problem
#include<stdio.h>
#include<stdlib.h>
struct Node{
	int data;
	struct Node *next;
};
void main(){
	struct Node *head=NULL;
	//First Node
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=10;
	newNode->next=NULL;

	//Connecting first node
	head=newNode;

	//Second node
	newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=20;
	newNode->next=NULL;

	//Connecting second node
	head->next=newNode;

	//Third Node
	newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=30;
	newNode->next=NULL;

	//Connecting third node
	head->next->next=newNode;
        
	//for printing LL all node condition temp->next
 	struct Node *temp=head;
	while(temp !=NULL){
		printf("%d ",temp->data);
		temp=temp->next;
	}
}

