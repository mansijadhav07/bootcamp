/*Implementing queue using linked list*/
#include<stdio.h>
#include<stdlib.h>
struct queue{
	int data;
	struct queue *next;
};
struct queue *front=NULL;
struct queue *rear=NULL;
int flag=0;
struct queue* createNode(){
	struct queue *newNode=(struct queue*)malloc(sizeof(struct queue));
	if(newNode==NULL){
		printf("Heap memory full\n");
		exit(0);//included in header file (#inclde<stdlib.h>)
	}
	else{
		printf("Enter data\n");
		scanf("%d",&newNode->data);
		newNode->next=NULL;
		return newNode;
	}
}
void enqueue(){
	struct queue *newNode=createNode();
	if(front==NULL){
		front=newNode;
		rear=newNode;
	}else{
		rear->next=newNode;
		rear=newNode;
	}
}
int dequeue(){
	if(front==NULL){
		flag=0;
		return -1;
	}else {
		flag=1;
		int val=front->data;
		if(front==rear){
		      free(front);
		      front=NULL;
		      rear=NULL;
		}else{
			struct queue *temp=front;
                        front=front->next;
			free(temp);
		}
		return val;
	}
}
int frontt(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		return front->data;
	}
}
int  printQueue(){
	if(front==NULL){
		return -1;
	}else{
		struct queue *temp=front;
	        while(temp!=NULL){
			printf("%d ",temp->data);
			temp=temp->next;
		}
		printf("\n");
		return 0;

	}
}




	
void main(){
	char choice;
	do{
		printf("1.enqueue\n2.dequeue\n3.frontt\n4.printQueue\n");
		int ch;
		printf("Make a choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				enqueue();
				break;
			case 2:{
				int ret=dequeue();
				if(flag==0){
					printf("Empty queue\n");
				}else{
					printf("%d is dequeued\n",ret);
				}
			       }
		                break;
			case 3:{

				int ret=frontt();
				if(flag==0){
					printf("Empty queue\n");
				}else{
					printf("Front element of queue =%d\n",ret);
				}
			       }
				break;
			case 4:{
				int ret=printQueue();
				if(ret==-1)
					printf("Empty queue\n");
				}
				break;
			default:
				printf("Wrong Choice\n");
				break;
		}getchar();
		printf("Do you want to continue?..Y/N\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
