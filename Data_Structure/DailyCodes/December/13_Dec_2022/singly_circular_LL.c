//Singly circular Linked List
#include<stdio.h>
#include<stdlib.h>
typedef struct Node{
	int data;
	struct Node *next;
}Node;
Node *head=NULL;
Node* createNode(){
	Node *newNode=(Node*)malloc(sizeof(Node));
	printf("Enter Data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
		newNode->next=head;
	}else{
		Node *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
	}
}
void addFirst(){
	Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
		newNode->next=head;
	}else{
		newNode->next=head;
		Node *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		head=newNode;
		temp->next=head;
	}
}
void addLast(){
	addNode();
}
int countNode(){
	int count=0;
	if(head==NULL){
		count=0;
		return count;
	}else{
	Node *temp=head;
	while(temp->next!=head){
		
		temp=temp->next;
	count++;
	}
	}
        
	return count+1;
}
int addAtPos(int pos){
	int count=countNode();
	if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addLast();
		}else{
			Node *newNode=createNode();
			Node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
		return 0;
	}
}
void deleteFirst(){
	if(head==NULL){
		printf("head is NULL Nothing to delete\n");
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			Node *temp=head;
			while(temp->next!=head){
				temp=temp->next;
			}
			temp->next=head->next;
			free(head);
			head=temp->next;
		}
	}
}
void deleteLast(){
       if(head==NULL){
                printf("head is NULL Nothing to delete\n");
        }else{
                if(head->next==head){
                        free(head);
                        head=NULL;
                }else{
                        Node *temp=head;
                        while(temp->next->next!=head){
                                temp=temp->next;
                        }
                        free(temp->next);
			temp->next=head;
                }
        }
}
int deleteAtPos(int pos){
	int count=countNode();
	if(pos<=0 || pos>count+1){
		printf("Nothing to delete\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			Node *temp1=head;
			Node *temp2=head;
			while(pos-2){
				temp2=temp2->next;
				pos--;
			}
			temp1=temp2->next;
			temp2->next=temp1->next;
			free(temp1);
		}
	}
}
void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
	Node *temp=head;
	while(temp->next!=head){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
	}
}

void main(){
	char choice;
	do{
		printf("1. Add Node\n");
		printf("2. Add First\n");
		printf("3. Add At Position\n");
		printf("4. Delete First\n");
		printf("5. Delete Last\n");
		printf("6. Delete At Position\n");
		printf("7. Count Nodes\n");
		printf("8. Print Linked List\n");

		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
                                addFirst();
                                break;
			case 3:
				{
					int pos;
					printf("Enter the position:\n");
					scanf("%d",&pos);
					addAtPos(pos);
				}
				break;
			case 4:
				deleteFirst();
				break;
                        case 5:
				deleteLast();
				break;
			case 6:{
				       int pos;
				       printf("Enter the position:\n");
				       scanf("%d",&pos);
				       deleteAtPos(pos);
			       }
			       break;
			case 7:
			       if(head==NULL){
				       printf("Linked list is empty\n");

			       }else{
			       printf("Count=%d",countNode());
			       }
			       break;
			case 8:
			       printLL();
			       break;
			default:
			       printf("Wrong Choice\n");
		}getchar();
		printf("\nDo you want to continue?\n");
		scanf("%c",&choice);
	}while(choice =='y' || choice =='Y');
}
