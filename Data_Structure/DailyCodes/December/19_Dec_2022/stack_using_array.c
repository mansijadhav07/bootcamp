//stack using array
#include<stdio.h>
#include<stdbool.h>
int top=-1;
int flag=0;
int size=0;
bool isFull(){
	if(top==size-1){
		return true;
	}else{
		return false;
	}
}
int push(int *stack){
	if(isFull()){
		return -1;
	}else{
		top++;
		printf("Enter data\n");
		scanf("%d",&stack[top]);
		return 0;
	}
}
bool isEmpty(){
	if(top==-1){
		return true;
	}else{
		return false;
	}
}
int pop(int *stack){
	if(isEmpty()){
		flag=0;
		return -1;
	}else{
		flag=1;
		int val=stack[top];
		top--;
		return val;
	}
}
int peek(int *stack){
	if(isEmpty()){
		flag=0;
		return -1;
	}else{
		flag=1;
		int val=stack[top];
		return val;
	}
}




void main(){
	
	printf("Enter size of stack:\n");
	scanf("%d",&size);
	int stack[size];
	char choice;
	do{
		printf("1.push\n2.pop\n3.peek\n");
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:{
				int ret=push(stack);
				if(ret==-1)
					printf("Stack Overflow\n");
				}
                                break;
			case 2:{
                                int ret=pop(stack);
				if(flag==0){
					printf("Stack Underflow\n");
				}else{
					printf("%d is popped\n",ret);
				}
			       }
			       break;
			case 3:{
				int ret=peek(stack);
				if(flag==0){
					printf("Stack is Empty\n");
				}else{
					printf("%d is returned\n",ret);
				}
			       }
				break;
			default:
				printf("Wrong choice\n");
				break;
		}getchar();
		printf("Do you want to continue?...Y/N\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
