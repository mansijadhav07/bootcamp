//Printing linked list correctly
#include<stdio.h>
#include<stdlib.h>
typedef struct Employee{
	char name[20];
	int id;
	struct Employee *next;
}emp;
emp *head=NULL;
emp* createNode(){
	emp *newNode=(emp*)malloc(sizeof(emp));
	getchar();//main \n in for loop
	printf("Enter name of employee:\n");
	char ch;
	int i=0;
	while((ch=getchar())!='\n'){
		(*newNode).name[i]=ch;
		i++;
	}
	printf("enter id:\n");
	scanf("%d",&newNode->id);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	emp *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		emp *temp=head;
		while(temp->next != NULL){
			temp=temp->next;
		}temp->next=newNode;
	}
}
void addFirst(){
	emp *newNode=createNode();
	if(head==NULL){
                head=newNode;
        }else{
               newNode->next=head;
	       head=newNode;
        }
}
void Count(){
	int count=0;
	emp *temp=head;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	printf("\nCount of nodes=%d\n",count);
}
void printLL(){
	emp *temp=head;
	while(temp!=NULL){
		printf("|%s",temp->name);
		printf(" %d|",temp->id);
		temp=temp->next;
	}
}
void addAtpos(){
	int pos;
	printf("Enter position:\n");
	scanf("%d",&pos);
	emp *newNode=createNode();
	emp *temp=head;
	while(pos-2){
		temp=temp->next;
		pos--;
	}
	newNode->next=temp->next;
	temp->next=newNode;
}
void main(){
	int nodeCount;
	int position;
	printf("Enter node Count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printf("\n");
	printLL();
	printf("\n");
	addFirst();
	printLL();
	printf("No of nodes:\n");
	Count();
	addAtpos();
	printLL();
}
