#include<stdio.h>
void main(){
	int size;
	printf("Enter size of array element:\n");
	scanf("%d",&size);
	int arr[size];
	int sum=0;
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("Array elements are:\n");
	for(int i=0;i<size;i++){
		printf("|%d|",arr[i]);
		sum=sum+arr[i];
	}
	printf("\nSum of array elements=%d\n",sum);
}
