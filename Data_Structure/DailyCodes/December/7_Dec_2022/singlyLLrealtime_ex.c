#include<stdio.h>
#include<stdlib.h>
typedef struct webseries{
	char wname[20];
	int episodes;
	float rating;
	struct webseries *next;
}web;
web *head=NULL;
web* createNode(){
	web *newNode=(web*)malloc(sizeof(web));
	getchar();
	printf("Enter webseries name:\n");
	int i=0;
	char ch;
	while((ch=getchar())!='\n'){
		(*newNode).wname[i]=ch;
		i++;
	}
	printf("Enter Episode count:\n");
	scanf("%d",&newNode->episodes);
	printf("Enter rating:\n");
	scanf("%f",&newNode->rating);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	web *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		web *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void addFirst(){
	web *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		newNode->next=head;
		head=newNode;
	}
}
void addLast(){
	addNode();
}
int countNode(){

	int count=0;
	web *temp=head;
	while(temp!=NULL){
		temp=temp->next;
		count++;
	}
	return count;
}
void addAtPos(int pos){
             
	int count=countNode();

	if(pos<=0 || pos>=count+2){
		printf("Invalid Position\n");
		return;
		
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addLast();
		}else{
			web *temp=head;
			web *newNode=createNode();
			while(pos-2){
			     temp=temp->next;
			     pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
	

	}
}
void deleteFirst(){
	int count=countNode();
	if(head==NULL){
		printf("Linked List is empty\n");
	}else if(count==1){
		free(head);
		head=NULL;
	}else{
		web *temp=head;
		head=temp->next;
		free(temp);
	}
}
void deleteLast(){
	int count=countNode();
	if(head==NULL){
		printf("Linked List is empty\n");
	}else if(count==1){
		free(head);
		head=NULL;
	}else{
		web *temp=head;
		while(temp->next->next!=NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}
void deleteAtPos(int pos){
	int count=countNode();
	if(pos<0 || pos>=count+2){
		printf("Invalid Position\n");
		
	}else{
		if(pos==1){
			deleteFirst();
                 }else if(pos==count){
			 deleteLast();
		 }else{
			 web *temp=head;
			 web *temp2=head;
			 while(pos-2){

				 temp=temp->next;
				 pos--;
			 }
			 temp2=temp->next;
			 temp->next=temp2->next;
			 free(temp2);
		 }
	}
}
void printLL(){
	web *temp=head;
	while(temp!=NULL){
		printf("|webseries name=%s",temp->wname);
		printf(" episodes=%d",temp->episodes);
		printf(" rating=%f|",temp->rating);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1. Add Node\n");
		printf("2. Add First\n");
		printf("3. Add At Position\n");
		printf("4. Delete First\n");
		printf("5. Delete Last\n");
		printf("6. Delete At Position\n");
		printf("7. Count Nodes\n");
		printf("8. Print Linked List\n");

		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
                                addFirst();
                                break;
			case 3:
				{
					int pos;
					printf("Enter the position:\n");
					scanf("%d",&pos);
					addAtPos(pos);
				}
				break;
			case 4:
				deleteFirst();
				break;
                        case 5: 
				deleteLast();
				break;
			case 6:{
				       int pos;
				       printf("Enter the position:\n");
				       scanf("%d",&pos);
				       deleteAtPos(pos);
			       }
			       break;
			case 7:
			       if(head==NULL){
				       printf("Linked list is empty\n");

			       }else{
			       printf("Count=%d",countNode());
			       }
			       break;
			case 8:
			       printLL();
			       break;
			default:
			       printf("Wrong Choice\n");
		}getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice =='y' || choice =='Y');
}


