//Doubly Linked list
#include<stdio.h>
#include<stdlib.h>
typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}Node;
Node *head=NULL;
Node *createNode(){
	Node *newNode=(Node*)malloc(sizeof(Node));
	newNode->prev=NULL;
	
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}

void addNode(){
	Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}

void addFirst(){
	Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		
		newNode->next=head;
		head->prev=newNode;
		head=newNode;
	}
}
void addLast(){
	addNode();
}
int countNode(){
	if(head==NULL){
		printf("Linked List is empty\n");
	}
	else{
		Node *temp=head;
	        int count=0;
		while(temp!=NULL){
			count++;
			temp=temp->next;
		}
		return count;
	}
}
int addAtpos(int pos){
	int count=countNode();
	if(pos<=0 || pos>count+1){
		printf("Invalid Position\n");
		return 0;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addLast();
		}else{
			Node *newNode=createNode();
			Node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
			newNode->prev=temp;
			newNode->next->prev=newNode;
		}
		return 0;
	}
}
int  deleteFirst(){
	if(head==NULL){
		printf("Nothing to delete\n");
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			head=head->next;
			free(head->prev);
			head->prev=NULL;
		}
		return 0;
	}
}
void deleteLast(){
	if(head==NULL){
		printf("Nothing to delete\n");
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			Node *temp=head;
			while(temp->next->next!=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
	}
}
int delAtPos(int pos){
	int count=countNode();
	if(pos<=0 || pos>count+1){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			Node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
		return 0;
	}
}


void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|",temp->data);
	}
}
void main(){
	char choice;
	do{
		printf("1. Add Node\n");
		printf("2. Add First\n");
		printf("3. Add At Position\n");
		printf("4. Delete First\n");
		printf("5. Delete Last\n");
		printf("6. Delete At Position\n");
		printf("7. Count Nodes\n");
		printf("8. Print Linked List\n");

		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
                                addFirst();
                                break;
			case 3:
				{
					int pos;
					printf("Enter the position:\n");
					scanf("%d",&pos);
					addAtpos(pos);
				}
				break;
			case 4:
				deleteFirst();
				break;
                        case 5: 
				deleteLast();
				break;
			case 6:{
				       int pos;
				       printf("Enter the position:\n");
				       scanf("%d",&pos);
				       delAtPos(pos);
			       }
			       break;
			case 7:
			       printf("Count=%d",countNode());
			       
			       break;
			case 8:
			       printLL();
			       break;
			default:
			       printf("Wrong Choice\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice =='y' || choice =='Y');
}


