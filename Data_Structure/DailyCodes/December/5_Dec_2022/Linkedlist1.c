#include<stdio.h>
#include<stdlib.h>
struct Node{
	int data;
	struct Node *next;
};
struct Node *head=NULL;
struct Node* createNode(){
	struct Node* newNode=(struct Node*)malloc(sizeof(struct Node));
	getchar();
	printf("Enter data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp=head;
		while(temp->next !=NULL){
			temp=temp->next;
		}temp->next=newNode;
	}
}
void addFirst(){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
	newNode->next=head;
	head=newNode;
	}
}
void addLast(){
	addNode();
}
int  countNode(){
	int count=0;
	struct Node *temp=head;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	printf("Count of nodes=%d",count);
	return count;
}
int addAtPos(int pos){
	int count=countNode();
	if(pos=count<0 ||pos>=count+2){
		printf("Invalid node position\n");
		return -1;
	}
	else {
		if(pos=count+1){
		addLast();
         	}else if(pos==1){
		  addFirst();
	        }else{
			struct Node *newNode=createNode();
			struct Node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
		return 0;
	}
}
void deleteFirst(){
	struct Node *temp=head;
	head=temp->next;
	free(temp);
}
void deleteLast(){
	struct Node *temp=head;
	while(temp->next->next!=NULL){
		temp=temp->next;
	}
	free(temp->next);
	temp->next=NULL;
}
void printLL(){
	struct Node *temp=head;
	while(temp!=NULL){
		printf("|%d|",temp->data);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.printLL\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addLast();
				break;
			case 4: 
				{
				int pos;
				printf("Enter position:\n");
				scanf("%d",&pos);
                                addAtPos(pos);
				}
				break;

			case 5:
				printLL();
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			default:
				printf("wrong choice\n");
		}getchar();
		printf("Do you want to continue\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
