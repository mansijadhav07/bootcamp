//Reverse Doubly linked list using swapping
#include<stdio.h>
void* malloc(size_t size);
void free(void *);
typedef struct Node{
	struct Node *prev;
	int data;
        struct Node *next;
}Node;
Node *head=NULL;
Node* createNode(){
	 Node *newNode=(Node*)malloc(sizeof( Node));

	newNode->prev=NULL;
	printf("Enter data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){

			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}

}
int countNode(){
	int count=0;
	Node *temp=head;
	while(temp!=NULL){
		temp=temp->next;
		count++;
	}
	return count;
}
void reverse(){
	if(head==NULL){
                printf("Empty Linked list\n");
	}else{
	Node *temp1=head;
	Node *temp2=head;
	while(temp2->next!=NULL){
		temp2=temp2->next;
	}
	int temp;
	int count=countNode();
	int cnt=count/2;
	while(cnt){
		temp=temp1->data;
		temp1->data=temp2->data;
		temp2->data=temp;
		temp1=temp1->next;
		temp2=temp2->prev;
		cnt--;
	}
	}
}


void printLL(){
	if(head==NULL){
		printf("Empty Linked List\n");
	}else{
		 Node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
			
		}
		printf("|%d|\n",temp->data);
	}
}
void main(){
	int nodeCount;
	printf("Enter node count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	reverse();
	printLL();
}
