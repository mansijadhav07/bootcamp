#include<stdio.h>
#include<stdlib.h>
typedef struct Demo{
	int data;
	struct Demo *next;
}Demo;
Demo *head=NULL;
Demo *createNode(){
	Demo *newNode=(Demo*)malloc(sizeof(Demo));
	printf("Enter data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;

}
void addNode(){
	Demo *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		Demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newNode;
	}
	
}
int countNode(){
	       int count=0;
	       Demo *temp=head;
	        while(temp!=NULL){
	                count++;
		       	temp=temp->next;
			
		}
		return count;
}
void reverse(){
        if(head==NULL){
                printf("Empty linked list\n");
        }else{
                int count=countNode();

                Demo *temp1=head;
               
                       int cnt=count/2;
		       while(cnt){
				Demo *temp2=head;
				int i=1;
                                while(i<count){
                                    temp2=temp2->next;
				    i++;
			       }
			       count--;
			       int temp;
                                        temp=temp1->data;
                                        temp1->data=temp2->data;
                                        temp2->data=temp;
                                        temp1=temp1->next;
                                        cnt--;
		       }
	}
}
                


void printLL(){
	Demo *temp=head;
	while(temp->next!=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
}
void main(){
	int node;
	printf("Enter node:\n");
	scanf("%d",&node);
	for(int i=1;i<=node;i++){
		addNode();
	}
	printLL();
	reverse();
	printLL();
}
