#include<stdio.h>
int factorial(int num){
	if(num<=1){
		return 1;
	}
	return factorial(num-1)*num;
}
void main(){
	int fact=factorial(4);
	printf("Factorial of 4 is %d\n",fact);
}
