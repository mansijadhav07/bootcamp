//WAP to print sum of array element by recursion
#include<stdio.h>
int arrSum(int *arr,int size){
	static int sum=0;
	
	if(size>=0){
		sum=sum+arr[size];
	}
	arrSum(arr,size-1);

	return sum;
}
void main(){
	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);	
	}
	int sum=arrSum(arr,size-1);
	printf("Sum of array elements is %d\n",sum);
}
