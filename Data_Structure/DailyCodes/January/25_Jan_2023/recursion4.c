//WAP to print 10 to 1 no by using tail recursion
#include<stdio.h>
void fun(int x){
	
	printf("%d\n",x);
	if(x!=1){
	fun(--x);
	}
}
void main(){
	fun(10);
}
