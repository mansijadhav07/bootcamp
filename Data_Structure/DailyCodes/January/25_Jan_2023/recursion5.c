//WAP to print sum of 10 to 1 numbers by recursion
#include<stdio.h>
int fun(int x){
	static int sum=0;
	sum=sum+x;
	if(x!=1){
		fun(--x);
	}
	return sum;
}
void main(){
	int sum=fun(10);
	printf("Sum=%d\n",sum);
}
