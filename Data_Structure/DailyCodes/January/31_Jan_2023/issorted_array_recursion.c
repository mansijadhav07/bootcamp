//WAP to check array is sorted or not using recursion
#include<stdio.h>
#include<stdbool.h>
bool isSorted(int arr[],int size,int index){
	if(index<size){
	if(arr[index]>arr[index+1]){
		return false;
	}
	if((arr[index]<arr[index+1]) && isSorted(arr,size,index+1)){
			return true;
	}
	}
}
void main(){
	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	bool ret=isSorted(arr,size-1,0);
	if(ret==true){
		printf("array is sorted\n");
	}else{
		printf("array is not sorted\n");
	}
}
