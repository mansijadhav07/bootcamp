//If count of even elements in array is greater than or equal to 2 return true otherwise false
#include<stdio.h>
int arrEven(int *arr,int size){
	int count=0;
	for(int i=0;i<=size;i++){
		if(arr[i]%2==0 && count<2){
			count++;
		}

	}
	return count;
}
void main(){
	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int ret=arrEven(arr,size-1);
	if(ret==2){
		printf("True\n");
	}else{
		printf("False\n");
	}
}
