//Compare string are equal or not until N  by recursion
#include<stdio.h>
#include<stdbool.h>
#include<string.h>
bool compare(char str1[],char str2[],int index,int N){
        if(index<=N){
        if((str1[index]==str2[index]) &&  compare(str1,str2,index+1,N)){
                        return true;
        }else{
                return false;
        }
        }
}
void main(){
        int size1,size2,N;
        printf("Enter size of string1:\n");
        scanf("%d",&size1);
        printf("Enter size of string2:\n");
        scanf("%d",&size2);
        char str1[size1];
        char str2[size2];
        printf("Enter string1\n");
        for(int i=0;i<size1;i++){
                scanf(" %c",&str1[i]);
        }
        printf("Enter string2\n");
        for(int i=0;i<size2;i++){
                scanf(" %c",&str2[i]);
        }
	printf("check string until:\n");
        scanf("%d",&N);	
        bool ret;
        ret=compare(str1,str2,0,N);
        if(ret==true){
                printf("strings are equal\n");
        }else{
                printf("Strings are not equal\n");
        }
}

