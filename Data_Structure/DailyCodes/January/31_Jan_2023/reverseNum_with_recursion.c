//Reverse number by using recursion
#include<stdio.h>
int reverseNum(int num,int r){
	if(num!=0){
		return	reverseNum(num/10,r*10+num%10);
	}
	if(num==0){
		return r;

	}
}
void main(){
	int num;
	printf("Enter number\n");
	scanf("%d",&num);
	int reverse=reverseNum(num,0);
	printf("The reverse of %d is %d\n",num,reverse);
}
