//WAP for count of zeros in input number
#include<stdio.h>
int countZero(int num){
	
        if(num==0){
           return 0;
        }
	if(num%10==0){
		return 1+countZero(num/10);
	}else{
	        return countZero(num/10);
	}
	
}

void main(){
        int input;
	int ret;
        printf("Enter number\n");
        scanf("%d",&input);
	if(input==0){
		 ret=1;
	}else{
                ret=countZero(input);
	}
        printf("Count of zeros in input number is %d\n",ret);
}

