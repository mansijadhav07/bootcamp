//If count of even elements in array is greater than or equal to 2 return true otherwise false
#include<stdio.h>
#include<stdbool.h>
bool arrEven(int *arr,int size,int count){
	if(count==2){
		return true;
	}
       	if(size>=0){
	      if(arr[size]%2==0){
	          return arrEven(arr,size-1,count+1);
	      }else{
	          return arrEven(arr,size-1,count+0);
	      }
        }
	return false;
}
void main(){
	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	bool ret=arrEven(arr,size-1,0);
	if(ret==true){
		printf("True\n");
	}else{
		printf("False\n");
	}
}
