#include<stdio.h>
int reverseNum(int num){
	int reverse=0;
	while(num){
		int rem=num%10;
		reverse=reverse*10+rem;
		num=num/10;
	}
	return reverse;
}
void main(){
	int num;
	printf("Enter number\n");
	scanf("%d",&num);
	int reverse=reverseNum(num);
	printf("The reverse of %d is %d\n",num,reverse);
}
