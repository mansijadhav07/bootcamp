#include<stdio.h>
int stepcount(int N){
	int count=0;
	while(N!=0){
		if(N%2==0){
			N=N/2;
			count++;
		}else{
			N=N-1;
			count++;
		}
	}
	return count;
}
void main(){
	int N;
	printf("Enter number\n");
	scanf("%d",&N);
	int ret=stepcount(N);
	printf("Total number of steps=%d\n",ret);
}
