//if input is even then divide by 2 and if input is odd then subtract by 1 
//until input becomes 0 how many steps are required return steps count
#include<stdio.h>
int stepcount(int N){
	if(N==0){
		return 0;
	}
	if(N%2==0){
		return stepcount(N/2)+1;
	}else{
		return stepcount(N-1)+1;
	}
}
void main(){
	int N;
	printf("Enter number\n");
	scanf("%d",&N);
	int ret=stepcount(N);
	printf("Total number of steps=%d\n",ret);
}
