//WAP for count of zeros in input number
#include<stdio.h>
int countZero(int num){
	int count=0;
	if(num==0){
		count++;
	}
	while(num!=0){
		int rem=num%10;
		if(rem==0){
			count++;
		}
		num=num/10;
	}
	return count;
}
void main(){
	int input;
	printf("Enter number\n");
	scanf("%d",&input);
	int ret=countZero(input);
	printf("Count of zeros in input number is %d\n",ret);
}
