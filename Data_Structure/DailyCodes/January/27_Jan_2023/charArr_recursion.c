//WAP to print element is present or not in array
#include<stdio.h>
int searchelement(char *carr,int size,char search){
	if(carr[size-1]==search){
		return size-1;
	}
	if(size==0){
		return -1;
	}
	return searchelement(carr,size-1, search);
}
void main(){
	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);
	char carr[size];
	printf("Enter array elements\n");
	for(char i=0;i<size;i++){
		scanf(" %c",&carr[i]);
	}
	char search;
	printf("Enter element to be searched\n");
	scanf(" %c",&search);
	int ret=searchelement(carr,size,search);
	if(ret==-1){
		printf("Element is not present in array\n");
	}else{
		printf("%c is present at %d\n",search,ret);
	}
}
