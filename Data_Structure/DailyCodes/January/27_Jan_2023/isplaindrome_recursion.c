#include<stdio.h>
#include<stdbool.h>
bool ispalindrome(char carr[],int start,int end){
	
	    if(start>=end){
		    return true;
	    }
	    if(carr[start]==carr[end] && ispalindrome(carr,start+1,end-1)){
			return true;
	    }
	    if(carr[start]!=carr[end]){
		    return false;
	    }
}
void main(){
	int size;
	printf("Enter size \n");
	scanf("%d",&size);
	char carr[size];
	printf("Enter string\n");
	for(char i=0;i<size;i++){
		scanf(" %c",&carr[i]);
	}
	//int start=0; 
	//start is always zero so we don't need start=0 variable instead of this we can directly pass 0 in function
        int end=size-1;
	bool ret=ispalindrome(carr,0,end);
	if(ret==true){
		printf("The string is palindrome\n");
	}else{
		printf("The string is not palindrome\n");
	}
}
