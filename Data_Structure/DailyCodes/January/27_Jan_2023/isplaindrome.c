#include<stdio.h>
#include<stdbool.h>
bool ispalindrome(char carr[],int size){
	int start=0;
	int end=size-1;
	while(start<end){
	    if(carr[start]==carr[end]){
			start++;
			end--;
	    }else{
		       return false;
	    }
	}
	return true;
}
void main(){
	int size;
	printf("Enter size \n");
	scanf("%d",&size);
	char carr[size];
	printf("Enter string\n");
	for(char i=0;i<size;i++){
		scanf(" %c",&carr[i]);
	}
	bool ret=ispalindrome(carr,size);
	if(ret==true){
		printf("The string is palindrome\n");
	}else{
		printf("The string is not palindrome\n");
	}
}
