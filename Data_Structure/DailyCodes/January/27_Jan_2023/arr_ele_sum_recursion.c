#include<stdio.h>
int arrSum(int *arr,int size){
	if(size==1){
		return arr[size-1];
	}
	return arrSum(arr,size-1)+arr[size-1];
}
void main(){
	int size=5;
        int arr[size];
	printf("Enter array element\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int sum=arrSum(arr,size);
	printf("Sum of array element is %d\n",sum);
}
