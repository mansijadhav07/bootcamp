//WAP to find element is present or not in character array
#include<stdio.h>
#include<stdbool.h>
bool searchele(char carr[],int size){
	char search;
	printf("Enter element to be search:");
	scanf(" %c",&search);
	for(char i=0;i<size;i++){
		if(carr[i]==search){
			return true;
		}
	}
	return false;
}
void main(){
	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);
	char carr[size];
	printf("Enter char array elements\n");
	for(char i=0;i<size;i++){
		scanf(" %c",&carr[i]);
	}
	bool ret=searchele(carr,size);
	if(ret==true){
		printf("Present\n");
	}else{
		printf("Absent\n");
	}
}
