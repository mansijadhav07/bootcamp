#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}Node;
Node *head=NULL;
Node* createNode(){
	Node *newNode=(Node*)malloc(sizeof(Node));
	newNode->prev=NULL;
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	
	}
}
void printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|",temp->data);
	}
}
void firstocc(){
	int num;
	printf("Enter data you want firstocc:\n");
	scanf("%d",&num);
	int occ=1;
	
	if(head==NULL){
		printf("linked list is empty\n");
	}else{
		Node *temp=head; 
	while(temp!=NULL){
		if(temp->data==num){
			printf("first occurance of %d is %d\n",num,occ);
			break;
		}
		occ++;
		temp=temp->next;
		
	}
	}
}

void main(){
	int node;
	printf("Enter node count\n");
	scanf("%d",&node);
	for(int i=1;i<=node;i++){
		addNode();
	}
	printLL();
	firstocc();
}
