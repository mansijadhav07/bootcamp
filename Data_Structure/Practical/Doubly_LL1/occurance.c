//Write a program that searches the occurance of a particular element from a doubly linked list
#include<stdio.h>
#include<stdlib.h>
struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
};
struct Node *head=NULL;
struct Node* createNode(){
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->prev=NULL;
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}
	else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
void occurance(){
	int occ=0;
        int num;
	printf("\nEnter element:\n");
	scanf("%d",&num);
	struct Node *temp=head;
	while(temp!=NULL){
		if(temp->data==num){
			occ++;
		}
		temp=temp->next;
	}
	if(occ==0){
		printf("No occurance of entered element\n");
	}else{
		printf("Occurance of %d is %d times in doubly linked list\n",num,occ);
        }
}
void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
	struct Node *temp=head;
	while(temp->next!=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
              }
	printf("|%d|",temp->data);
        }
}

void main(){
	int nodeCount;
	printf("enter number of nodes:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	occurance();
}
