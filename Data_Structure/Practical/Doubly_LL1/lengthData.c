//Write a program that accept a doubly linked list from the user.Take a number from user and print the data of that number
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct Node{
	struct Node *prev;
	char str[20];
	struct Node *next;
};
struct Node *head=NULL;
struct Node* createNode(){
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->prev=NULL;
	getchar();
	printf("Enter string\n");
	int i=0;
	char ch;
	while((ch=getchar())!='\n'){
		(*newNode).str[i]=ch;
		i++;
	}
	
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}
	else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
void length(){
	int length;
	printf("\nEnter length of string\n");
	scanf("%d",&length);
	struct Node *temp=head;
	while(temp!=NULL){
		if((strlen(temp->str))==length){
			printf("%s",temp->str);
		}
		temp=temp->next;
	}
	
}
void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
	struct Node *temp=head;
	while(temp->next!=NULL){
		printf("|%s|->",temp->str);
		temp=temp->next;
              }
	printf("|%s|",temp->str);
        }
}

void main(){
	int nodeCount;
	printf("enter number of nodes:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	length();
}
