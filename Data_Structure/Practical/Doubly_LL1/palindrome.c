
#include<stdio.h>
#include<stdlib.h>
struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
};
struct Node *head=NULL;
struct Node* createNode(){
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->prev=NULL;
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}
	else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
void palindrome(){
        int pos=1;
	struct Node *temp=head;
	while(temp!=NULL){
		int rem;
		int reverse=0;
                int original=temp->data;
		while(original!=0){
		       rem=original%10;
		       reverse=reverse*10+rem;
		       original=original/10;
		}
	        if(temp->data==reverse){
	                           
			printf("\nPalindrome present at %d",pos);
		}
		pos++;
		
		temp=temp->next;
	}
}
void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
	struct Node *temp=head;
	while(temp->next!=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
              }
	printf("|%d|",temp->data);
        }
}

void main(){
	int nodeCount;
	printf("enter number of nodes:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	palindrome();
	
}
