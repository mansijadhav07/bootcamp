//Write a program that adds the digits of a data element from a doubly linked list and changes the data
//input linked list : |11|->|12|->|13|->|141|->|2|->|158|
//output linked list: |2|->|3|->|4|->|6|->|2|->|14|
#include<stdio.h>
#include<stdlib.h>
typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;
node *head=NULL;
node* createNode(){
	node *newNode=(node*)malloc(sizeof(node));
	newNode->prev=NULL;
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}
	else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
void printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
	}else{
	node *temp=head;
	while(temp->next!=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
	}
}
int sumOfDataEle(){
	node *temp=head;
	
	while(temp!=NULL){
		int sum=0;
		int rem;
		while(temp->data!=0){
			rem=temp->data%10;
			sum=sum+rem;
			temp->data=temp->data/10;
		}
                temp->data=sum;
		temp=temp->next;
	}
}


void main(){
	int nodeCount;
	printf("Enter node count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	sumOfDataEle();
	printf("\n");
	printLL();
}
