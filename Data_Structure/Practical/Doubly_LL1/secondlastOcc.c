// Write a program that searches for the second last occurance of a particular element from a doubly linked list
#include<stdio.h>
#include<stdlib.h>
struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
};
struct Node *head=NULL;
struct Node* createNode(){
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->prev=NULL;
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
void printLL(){
	if(head==NULL){
		printf("Empty linked list\n");
	}else{
	struct Node *temp=head;
	while(temp->next!=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
	}
}
void secondLastOcc(){
	int num;
	printf("\nEnter element\n");
	scanf("%d",&num);
	int cur=1;
	int first=0;
	int second=0;
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
		struct Node *temp=head;
		while(temp!=NULL){
			if(temp->data==num){
			second=first;
			first=cur;
		       }
		cur++;
		temp=temp->next;
		}
	}
	if(first==0 && second==0){
		printf("No occurance of entered element\n");
	}else if(first>0 && second==0){
		printf("Singly occurance\n");
	}else{
		printf("Second last occurance of %d is %d\n",num,second);
        }
}

void main(){
	int nodeCount;
	printf("Enter node count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	secondLastOcc();
}
