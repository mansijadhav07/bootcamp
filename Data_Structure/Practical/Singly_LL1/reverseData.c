/*WAP that accepts a singly linear linked list from the user.Reverse the data elements from linked list*/
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	char str[20];
	struct node *next;
}node;
node *head=NULL;
node *createNode(){
	node *newNode=(node*)malloc(sizeof(node));
	int i=0;
	char ch;
	
	printf("Enter string:\n");
	while((ch=getchar())!='\n'){
		(*newNode).str[i]=ch;
		i++;
	}
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	if(head==NULL){
	      printf("Empty linked list\n");
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			printf("|%s|->",temp->str);
			temp=temp->next;
		}
		printf("|%s|",temp->str);
	}
}
char* mystrrev(char *str){
	char *temp=str;
	while(*temp!='\0'){
		temp++;
	}
	temp--;
	char x;
	while(str<temp){
		x=*str;
		*str=*temp;
		*temp=x;
		str++;
		temp--;
	}
	return str;
}
void reverse(){
	char string[20];
	node *temp=head;
	while(temp!=NULL)
	{
		mystrrev(temp->str);
	        printf("%s ",temp->str);
	        temp=temp->next;
        }
	
}

void main(){
	int nodeCount;
	printf("Enter node count:\n");
	scanf("%d",&nodeCount);
	
	getchar();

	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	printf("\n");
	reverse();
	
}
