/*WAP that accepts a singly linear linked list from the user.Take a number from the user and print the data of length of that number*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct node{
	char str[20];
	struct node *next;
}node;
node *head=NULL;
node *createNode(){
	node *newNode=(node*)malloc(sizeof(node));
	int i=0;
	char ch;
	printf("Enter string:\n");
	while((ch=getchar())!='\n'){
		(*newNode).str[i]=ch;
		i++;
	}
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	if(head==NULL){
	      printf("Empty linked list\n");
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			printf("|%s|->",temp->str);
			temp=temp->next;
		}
		printf("|%s|",temp->str);
	}
}
void length(){
	int len;
	int c;
	printf("\nEnter length\n");
	scanf("%d",&len);
	node *temp=head;
	while(temp!=NULL){
		 c=strlen(temp->str);
                if(c==len){
		printf("%s\n",temp->str);
	        } 
	        temp=temp->next;
}
}

void main(){
	int nodeCount;
	printf("Enter node count:\n");
	scanf("%d",&nodeCount);
	getchar();
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	length();
	
}
