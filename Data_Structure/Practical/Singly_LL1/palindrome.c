/*WAP that searches all the palindrome data elements from a singly linear linked list and print position of palindrome*/
#include<stdio.h>
#include<stdlib.h>
typedef struct Node{
	int data;
	struct Node *next;
}node;
node *head=NULL;
node* createNode(){
	node *newNode=(node*)malloc(sizeof(node));
	getchar();
	printf("Enter integer data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
	node *temp=head;
        while(temp->next!=NULL){
	    printf("|%d|->",temp->data);
            temp=temp->next;
         }
	printf("|%d|",temp->data);
      }
}
void palindrome(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
        int pos=1;
	node *temp=head;
	
	while(temp!=NULL){
                int rem;
		int reverse=0;
		int original=temp->data;
		while(original!=0){
		rem=original%10;
		reverse=reverse*10+rem;
		original=original/10;
		}
		if(temp->data==reverse){
			printf("Palindrome present at %d \n",pos);
		}
		pos++;
		
		
		temp=temp->next;
        }
    }

}
void main(){
	int nodeCount;
	printf("Enter Node Count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	printf("\n");
	palindrome();
	
}
