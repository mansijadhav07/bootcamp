/*WAP that searches for the second last occurance of a particular element from singly linear linked list*/
#include<stdio.h>
#include<stdlib.h>
typedef struct Node{
	int data;
	struct Node *next;
}node;
node *head=NULL;
node* createNode(){
	node *newNode=(node*)malloc(sizeof(node));
	getchar();
	printf("Enter integer data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
	node *temp=head;
        while(temp->next!=NULL){
	    printf("|%d|->",temp->data);
            temp=temp->next;
         }
	printf("|%d|",temp->data);
      }
}
void secondLastOccurance(){
	int num;
	printf("\nEnter element:\n");
	scanf("%d",&num);
	int curpos=1;
	int first=0;
	int second=0;
	

	if(head==NULL){
		printf("linked list is empty\n");
	}else{
		node *temp=head;
		while(temp!=NULL){
			if(temp->data==num){
			second=first;
			first=curpos;
			}
			curpos++;
			temp=temp->next;
		}

	}
	if(second==0 && first==0){
		printf("No occurance of the entered number\n");
	}else if(first>0 && second==0){
		printf("Singly occurance\n");
	}else{
		printf("Second last occurance of %d is %d in singly linked list\n",num,second);
	}

}

	


void main(){
	int nodeCount;
	printf("Enter Node Count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	secondLastOccurance();
}
