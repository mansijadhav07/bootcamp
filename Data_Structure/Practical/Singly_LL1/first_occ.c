/*WAP that searches for the first occurance of a particular element from singly linear linked list*/
#include<stdio.h>
#include<stdlib.h>
typedef struct Node{
	int data;
	struct Node *next;
}node;
node *head=NULL;
node* createNode(){
	node *newNode=(node*)malloc(sizeof(node));
	getchar();
	printf("Enter integer data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
	node *temp=head;
        while(temp->next!=NULL){
	    printf("|%d|->",temp->data);
            temp=temp->next;
         }
	printf("|%d|",temp->data);
      }
}
void firstOccurance(){
	int num;
	printf("\nEnter element:\n");
	scanf("%d",&num);
	int occ=1;
	node *temp=head;

	if(head==NULL){
		printf("linked list is empty\n");
	}else{
		while(temp!=NULL){
			if(temp->data==num){
			printf("First occ of %d is %d\n",num,occ);
	                break;
			}
			occ++;
			temp=temp->next;
		}

	}

}

	


void main(){
	int nodeCount;
	printf("Enter Node Count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	firstOccurance();
}
