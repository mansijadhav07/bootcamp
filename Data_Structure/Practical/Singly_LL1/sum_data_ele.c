/*WAP that adds the digits of a data element from a singly linear linked list and changes the data(sum of data element digits)*/
#include<stdio.h>
#include<stdlib.h>
typedef struct Node{
	int data;
	struct Node *next;
}node;
node *head=NULL;
node* createNode(){
	node *newNode=(node*)malloc(sizeof(node));
	getchar();
	printf("Enter integer data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
	node *temp=head;
        while(temp->next!=NULL){
	    printf("|%d|->",temp->data);
            temp=temp->next;
         }
	printf("|%d|",temp->data);
      }
}
void sumElement(){
	node *temp=head;
	int rem;
	while(temp!=NULL){
		int sum=0;
		int num=temp->data;
		while(num!=0){
		rem=num%10;
		sum=sum+rem;
		num=num/10;
		}
		temp->data=sum;
		
		temp=temp->next;
        }
}


	


void main(){
	int nodeCount;
	printf("Enter Node Count:\n");
	scanf("%d",&nodeCount);
	for(int i=1;i<=nodeCount;i++){
		addNode();
	}
	printLL();
	printf("\n");
	sumElement();
	printLL();
}
