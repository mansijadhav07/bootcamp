/*WAP for the linked list of festivals in India
 take input from user in linked list and print its data*/
#include<stdio.h>
#include<stdlib.h>
typedef struct festival{
	char fname[20];
	int days;
	float rangoli;
	struct festival *next;
}fest;
fest *head=NULL;
void addNode(){
	fest *newNode=(fest*)malloc(sizeof(fest));
	printf("Enter name of festival:\n");
	fgets(newNode->fname,15,stdin);
	printf("Enter how many holiday for festival:\n");
	scanf("%d",&newNode->days);
	printf("Enter rangoli used:\n");
	scanf("%f",&newNode->rangoli);
	newNode->next=NULL;
	getchar();
	if(head==NULL){
		head=newNode;
	}else{
		fest *temp=head;
		while(temp->next!=NULL){
			 temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	fest *temp=head;
	while(temp!=NULL){
		printf("%s",temp->fname);
		printf("%d\n",temp->days);
		printf("%f\n",temp->rangoli);
		temp=temp->next;
	}
}
void main(){
	addNode();
	addNode();
	addNode();

	printLL();
}
