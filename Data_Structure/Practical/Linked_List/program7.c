/*Print the maximum integer data from above nodes*/
#include<stdio.h>
#include<stdlib.h>
struct demo{
	int data;
	struct demo *next;
};
struct demo *head=NULL;
void addNode(){
	struct demo *newNode=(struct demo*)malloc(sizeof(struct demo));
	printf("Enter integer data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	if(head==NULL){
		head=newNode;
	}else{
		struct demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void max(){
	
	struct demo *temp=head;
	int max=temp->data;
	while(temp!=NULL){
		if(temp->data>max){
		max=temp->data;
	       	}
		temp=temp->next;
	}
	printf("Maximum integer data =%d\n",max);

}
void main(){
	addNode();
	addNode();
        addNode();

	max();
}

