/*WAP for the Linked list states in India consisting of its name,population,budget and literacy
connect 4 states in the Linked List and print their data*/
#include<stdio.h>
#include<stdlib.h>
typedef struct states{
	char sname[20];
	long population;
	double budget;
	float literacy;
	struct states *next;
}state;
state *head=NULL;
void addNode(){
	state *newNode=(state*)malloc(sizeof(state));
	printf("Enter State name:\n");
	fgets(newNode->sname,15,stdin);
	printf("Enter State Population:\n");
	scanf("%ld",&newNode->population);
	printf("Enter State Budget:\n");
        scanf("%lf",&newNode->budget);
	printf("Enter State literacy:\n");
        scanf("%f",&newNode->literacy);
	getchar();
	newNode->next=NULL;
	if(head==NULL){
		head=newNode;
	}else{
		state *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	state *temp=head;
                while(temp!=NULL){
			printf("%s",temp->sname);
			printf("%ld ",temp->population);
                        printf("%lf ",temp->budget);
			printf("%f\n",temp->literacy);
			temp=temp->next;
                }
}
void main(){
	addNode();
	addNode();
        addNode();
        addNode();

	printLL();
}




