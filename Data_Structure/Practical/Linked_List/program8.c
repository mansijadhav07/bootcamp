/*Print the minimum integer data from above nodes*/
#include<stdio.h>
#include<stdlib.h>
struct demo{
	int data;
	struct demo *next;
};
struct demo *head=NULL;
void addNode(){
	struct demo *newNode=(struct demo*)malloc(sizeof(struct demo));
	printf("Enter integer data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	if(head==NULL){
		head=newNode;
	}else{
		struct demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void min(){
	
	struct demo *temp=head;
	int min=temp->data;
	while(temp!=NULL){
		if(temp->data<min){
		min=temp->data;
	       	}
		temp=temp->next;
	}
	printf("Minimum integer data =%d\n",min);

}
void main(){
	addNode();
	addNode();
        addNode();

	min();
}

