/*WAP for the linked list of malls consisting of its name,number of shops  and revenue
 * connect 3 malls in the linked list and print their data*/
#include<stdio.h>
#include<stdlib.h>
typedef struct malls{
	char mName[15];
	int noOfshop;
	float revenue;
	struct malls *next;
}mall;
mall *head=NULL;
void addNode(){
	mall *newNode=(mall*)malloc(sizeof(mall));
	printf("Enter mall name:\n");
	fgets(newNode->mName,10,stdin);
	printf("Enter no of shop:\n");
	scanf("%d",&newNode->noOfshop);
	printf("Enter revenue:\n");
	scanf("%f",&newNode->revenue);
	getchar();
	newNode->next=NULL;
	if(head==NULL){
		head=newNode;
	}else{
		mall *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	mall *temp=head;
                while(temp!=NULL){
			printf("%s ",temp->mName);
			
			printf(" %d ",temp->noOfshop);
			printf("%f\n",temp->revenue);

                        temp=temp->next;
                }
                
}


void main(){
	addNode();
	addNode();
        addNode();
         
	printLL();
}
