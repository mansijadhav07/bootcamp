/*WAP to count the number of festival nodes in the above program*/
#include<stdio.h>
#include<stdlib.h>
typedef struct festival{
	char fname[20];
	int days;
	float rangoli;
	struct festival *next;
}fest;
fest *head=NULL;
void addNode(){
	fest *newNode=(fest*)malloc(sizeof(fest));
	printf("Enter name of festival:\n");
	fgets(newNode->fname,15,stdin);
	printf("Enter how many holiday for festival:\n");
	scanf("%d",&newNode->days);
	printf("Enter rangoli used:\n");
	scanf("%f",&newNode->rangoli);
	newNode->next=NULL;
	getchar();
	if(head==NULL){
		head=newNode;
	}else{
		fest *temp=head;
		while(temp->next!=NULL){
			 temp=temp->next;
		}
		temp->next=newNode;
	}
}
void counter(){
	fest *temp=head;
	int count=0;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	printf("Count of nodes in linked list=%d\n",count);
}
void main(){
	addNode();
	addNode();
	addNode();

	counter();
}
