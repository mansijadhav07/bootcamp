/*write a real time example for a linked list and print its data
 * take 5 nodes from user*/
#include<stdio.h>
#include<stdlib.h>
typedef struct WebSeries{
	char Wname[20];
	int season;
	float rating;
	struct WebSeries *next;
}web;
web *head=NULL;
void addNode(){
	web *newNode=(web*)malloc(sizeof(web));
	getchar();
	char ch;
	
	printf("Enter webseries name:\n");
	int i=0;
	while((ch=getchar())!='\n'){
              (*newNode).Wname[i]=ch;
	      i++;

	}
	printf("Enter webseries season\n");
	scanf("%d",&newNode->season);
	printf("Enter webseries rating\n");
	scanf("%f",&newNode->rating);
	newNode->next=NULL;
	if(head==NULL){
		head=newNode;
	}else{
		web *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){
	web *temp=head;
	while(temp!=NULL){
	printf("|%s ",temp->Wname);
	printf("%d ",temp->season);
	printf("%f|",temp->rating);
	temp=temp->next;
	}
}
void main(){
	int nodecount;
	printf("Enter node count:\n");
	scanf("%d",&nodecount);
	for(int i=1;i<=nodecount;i++){
		addNode();
	}
	printLL();
}




