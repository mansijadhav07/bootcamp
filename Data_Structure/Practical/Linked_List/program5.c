/*Write a demo structure consisting of integer data
 * Take the number of nodes from the user and print the addition of integer data*/
#include<stdio.h>
#include<stdlib.h>
struct demo{
	int data;
	struct demo *next;
};
struct demo *head=NULL;
void addNode(){
	struct demo *newNode=(struct demo*)malloc(sizeof(struct demo));
	printf("Enter integer data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	if(head==NULL){
		head=newNode;
	}else{
		struct demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void addData(){
	int add=0;
	struct demo *temp=head;
	while(temp!=NULL){
		add=temp->data+add;
		temp=temp->next;
	}
	printf("Addition of int data in Liked list nodes=%d\n",add);
}
void main(){
	addNode();
	addNode();
        addNode();

	addData();
}

