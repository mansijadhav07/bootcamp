/*Print addition of first and last node data from the above code*/
#include<stdio.h>
#include<stdlib.h>
struct demo{
	int data;
	struct demo *next;
};
struct demo *head=NULL;
void addNode(){
	struct demo *newNode=(struct demo*)malloc(sizeof(struct demo));
	printf("Enter integer data:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	if(head==NULL){
		head=newNode;
	}else{
		struct demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void addFirstLast(){
	int add=0;
	struct demo *temp=head;
	while(temp!=NULL){
		if(temp==head || temp->next==NULL){
		add=temp->data+add;
	       	}
		temp=temp->next;
	}
	printf("Addition of int data in Liked list nodes=%d\n",add);

}
void main(){
	addNode();
	addNode();
        addNode();

	addFirstLast();
}

