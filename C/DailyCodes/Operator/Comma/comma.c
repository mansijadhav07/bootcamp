/* Comma is binary operator when used in round bracket last value is assigned to variable*/
#include<stdio.h>
void main(){
//	int x=10,20,30;
//	printf("%d\n",x);//error: expected identifier 

	int y={10,20,30};
	printf("%d\n",y);//10
        //warning: excess elements in scalar initializer 20,30


	int z=(10,20,30);
	printf("%d\n",z);//30
}
