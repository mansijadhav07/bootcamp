//String Library Function-strlen(string length)
//prototype : int strlen(char*);
#include<stdio.h>
#include<string.h>
void main(){
	char Name[10]={'K','L','R','a','h','u','l','\0'};
	char *str="Virat Kohli";
	int lenName=strlen(Name);
	int lenstr=strlen(str);
	printf("length of Name is %d\n",lenName);
	printf("length of str is %d\n",lenstr);
}


