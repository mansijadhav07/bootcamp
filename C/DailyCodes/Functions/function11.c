//Default Parameter
#include<stdio.h>
void fun(int,int,int);
void main(){
	int x=10;
	int y=20;
	int z=30;
	fun(x,y,z);//60
	//fun(x,y);//error: too few arguments to function ‘fun’
}
void fun(int x,int y,int z){
             printf("Sum=%d\n",x+y+z);
}	     
/*
 expected ‘;’, ‘,’ or ‘)’ before ‘=’ token
   void fun(int x,int y,int z=30) default parameter are not allowed in C language
*/
