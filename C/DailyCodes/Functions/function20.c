//Sum of diagoanl element
#include<stdio.h>
int diagSum1(int arr[][3],int row,int col){
	int sum=0;
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			if(i==j){
				sum=sum+arr[i][j];
			}
		}
	}
	return sum;
}
int diagSum2(int (*ptr)[],int arrSize){ //(*ptr)[3]
	int sum=0;
	for(int i=0;i<arrSize;i++){
		if(i%4==0){
			sum=sum+*(*ptr+i);

		}
	}
	return sum;
}
void main(){
	int arr[3][3]={1,2,3,4,5,6,7,8,9};
	int arrSize=sizeof(arr)/sizeof(int);
	int sum1=diagSum1(arr,3,3);
	printf("Sum of diagonal element is %d\n",sum1);
	int sum2=diagSum2(arr,arrSize);
	printf("Sum of diagonal element is %d\n",sum2);
}
