#include<stdio.h>
//Pass float as parameter
void fun(float x){
        printf("%f\n",x);
}
void main(){
        fun('A');//65.000000
        fun(10);//10.000000
        fun(20.5f);//20.500000
        fun(35.50);//35.500000
}


