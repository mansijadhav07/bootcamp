//WAP to return sum of array element from function

#include<stdio.h>
int sumOfArray(int *ptr,int size){
	int sum=0;
	for(int i=0;i<size;i++){
		sum=sum+*(ptr+i);
	}
	return sum;
}
void main(){
	int arr[]={10,20,30,40,50};
	int size=sizeof(arr)/sizeof(arr[0]);
	int sum=sumOfArray(arr,size);
	printf("Sum of array elements=%d\n",sum);
}
