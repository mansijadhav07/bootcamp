#include<stdio.h>
//arr-first element address in 1D array
//&arr-whole array address 
//arr and &arr is same because address of first element and address of whole array is same
void funarr(int *arr){
	printf("%p\n",arr);
}
void main(){
	int arr[]={10,20,30,40};
	funarr(arr);//0x100 address of first element of an array
	funarr(&arr[1]);//0x104
}
