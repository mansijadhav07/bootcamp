//Passing array to function
#include<stdio.h>
void printArr1(int *ptr,int size){
	printf("PrintArr1\n");
	for(int i=0;i<size;i++){
		printf("%d\n",*(ptr+i));
	}
}
void printArr2(int arr[],int size){
         printf("PrintArr2\n");
         for(int i=0;i<size;i++){
	      printf("%d\n",arr[i]);
	 }
}
void main(){
	int arr[]={10,20,30,40,50};
	int size=sizeof(arr)/sizeof(arr[0]);
	printArr1(arr,size);
	printArr2(arr,size);
}
