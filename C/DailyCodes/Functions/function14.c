#include<stdio.h>
void swap(int*,int*);
void main(){
	printf("Start Main\n");
        int x=10;
        int y=20;
        printf("x=%d\n",x);//10
        printf("y=%d\n",y);//20
        printf("Start swap\n");
        swap(&x,&y);
	printf("End swap\n");
        printf("x=%d\n",x);//20
        printf("y=%d\n",y);//10
        printf("End Main\n");
}
void swap(int *x,int *y){
         printf("x=%d\n",*x);//10
         printf("y=%d\n",*y);//20

         int temp;
            temp=*x;
            *x=*y;
            *y=temp;

        printf("x=%d\n",*x);//20
        printf("y=%d\n",*y);//10
}


