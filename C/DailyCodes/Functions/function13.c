//In C call by address and call by reference is same
#include<stdio.h>
void callByAddress(int *ptr);
void main(){
	int x=10;
	printf("x=%d\n",x);//10
	callByAddress(&x);
	printf("x=%d\n",x);//50
}
void callByAddress(int *ptr){
	printf("%p\n",ptr);//0x100
	printf("%d\n",*ptr);//10
	*ptr=50;
	printf("%d\n",*ptr);//50
}
