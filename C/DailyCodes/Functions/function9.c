#include<stdio.h>
void  fun(int x);
void main(){
        fun(10);//10
        fun('A');//65
}

void  fun(int x){//conflicting types for ‘fun’; have ‘void(int)’
                 // if type int fun(int x) removes above warning
	printf("%d\n",x);
}
/*
void main(){
        fun(10);//10 warning: implicit declaration of function ‘fun’ 
        fun('A');//65
}

void  fun(int x){
        printf("%d\n",x);
}

