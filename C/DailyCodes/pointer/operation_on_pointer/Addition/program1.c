#include<stdio.h>
void main(){
	int x=10;
	char ch='A';

	int *ptr1=&x;
	char *ptr2=&ch;

        printf("Address of x=%p\n",&x);
       	printf("Address of ptr1=%p\n",ptr1);
	printf("\n");

	printf("Address of ch=%p\n",&ch);
	printf("Address of ptr2=%p\n",ptr2);
	printf("\n");

	printf("Value at ptr1=%d\n",*ptr1);
	printf("Value at ptr2=%d\n",*ptr2);
}
