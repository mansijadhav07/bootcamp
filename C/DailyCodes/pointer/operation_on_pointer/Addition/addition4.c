/*  IMP PROGRAM
    POINTER AND INTEGER ADDITION IS ALLOWED
    EX. *(ptr+1)
    add of ptr=100
    *(ptr+1)
    =*(100+1*DTP)    DTP=data type of pointer
    =*(100+1*4)
    =*(104)
    =value at(104)
    =20
*/
#include<stdio.h>
void main(){
	int x=10;//100
	int y=20;//104

	int *ptr=&x;//104
	printf("address of x=%p\n",&x);
	printf("Address of ptr=%p\n",ptr);
	printf("Address of ptr+1=%p\n",(ptr+1));
	printf("\n");
	printf("Value at *ptr=%d\n",*ptr);//10
        printf("Value at *(ptr1+1)=%d\n",*(ptr+1));//20
}
