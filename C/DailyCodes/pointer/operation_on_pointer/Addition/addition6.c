#include<stdio.h>
void main(){
	char ch1='x';
	char ch2='y';

	char *ptr=&ch1;

	printf("Address of ptr=%p\n",ptr);//100
	printf("Value at *ptr=%c\n",*ptr);//x
        printf("\n");
	//printf("Address of (ptr+1.5)=%p\n",(ptr+1.5));//error
       //printf("Value at *(ptr+1.5)=%c\n",*(ptr+1.5));//error
	printf("\n");
	printf("Address of (ptr+'A')=%p\n",(ptr+'A'));//100+65=165
	printf("Value at  *(ptr+'A')=%c\n",*(ptr+'A'));//segmentation fault or garbage value
}
