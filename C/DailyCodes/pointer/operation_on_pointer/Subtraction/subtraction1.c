#include<stdio.h>
void main(){
	int arr[]={10,20,30,40};
	int *ptr1=&(arr[1]);
	printf("Value at ptr1=%d\n",*ptr1);
	printf("Value at *(ptr1+2)=%d\n",*(ptr1+2));
}
/* 10 20 30 40
   *ptr1=20
   *(ptr1+2)
   =*(100+2*DTP)
   =*(100+2*4)
   =*(108)
   =40
 */
