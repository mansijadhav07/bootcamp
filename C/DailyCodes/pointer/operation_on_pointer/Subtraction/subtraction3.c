/* Pointer subtraction is allowed
   pointer subtraction is meaningful if it will point elements in same array
   value of subtraction of pointer is doubel so use %ld
   ptr2-ptr1
   =(ptr2-ptr1)/sizeof(DTP) 
   =(112-100)/4
   =3
 */
#include<stdio.h>
void main(){
	int arr[]={10,20,30,40,50};
	int *ptr1=&(arr[0]);
	int *ptr2=&(arr[3]);

	printf("Value at *ptr1=%d\n",*ptr1);
	printf("Value at *ptr2=%d\n",*ptr2);
	//Subtraction of pointer
	printf("Value of ptr2-ptr1=%d\n",ptr2-ptr1);//warning 
	printf("Value of ptr1-ptr2=%d\n",ptr1-ptr2);//warning
	printf("Value of ptr2-ptr1=%ld\n",ptr2-ptr1);
	printf("Value of ptr1-ptr2=%ld\n",ptr1-ptr2);
}

