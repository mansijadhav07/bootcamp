#include<stdio.h>
void main(){
	char carr[]={'A','B','C','D'};
	char *cptr1=&carr[2];
	char *cptr2=&carr[3];
	printf("value at *cptr1=%c\n",*cptr1);
	printf("Value at *cptr2=%c\n",*cptr2);
	printf("%c",*(cptr1+2));//garbage value or segmentation fault
	printf("%c",*(cptr2+1));//garbage value or segmentation fault
}
