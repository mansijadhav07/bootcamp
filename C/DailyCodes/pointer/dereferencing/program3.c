#include<stdio.h>
//data section global varible
int a=10;
char b='A';
void main(){
	float x=25.5;
	double d=35.5;

	int *ptr1=&a;
	char *ptr2=b;
	float *ptr3=&x; 
        double  *ptr4=&d;

	printf("Address of ptr1=%p\n",ptr1);//400
	printf("Address of ptr2=%p\n",ptr2);//65
	printf("Address of ptr3=%p\n",ptr3);//100
	printf("Address of ptr4=%p\n",ptr4);//104

	printf("Value at ptr1=%d\n",*ptr1);//10
	printf("Value at ptr2=%c\n",*ptr2);//segmentation fault
	//After segmentation fault no line will be printed
	printf("Value at ptr3=%f\n",*ptr3);
	printf("Value at ptr4=%lf\n",*ptr4);
}

