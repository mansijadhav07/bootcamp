#include<stdio.h>
void main(){
	int x=10;

	int *ptr1=&x;//100
	int *ptr2=x;//10

	printf("Address of x =%p\n",&x);//0x100
        printf("Address of ptr1=%p\n",ptr1);//0x100
        printf("Address of ptr2=%p\n",ptr2);//0xa=hexadecimal value of 10 (10) 

	printf("value at ptr1=%d\n",*ptr1);//10
	printf("value at ptr2=%d\n",*ptr2);//segmentation fault
}
