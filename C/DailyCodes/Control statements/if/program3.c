#include<stdio.h>
void main(){
	printf("Start main\n");
	int x=0;
	int y=20;
	if(x){
		printf("In first if block\n");
	}
	if(y){
		printf("In second if block\n");
	}
	printf("End main\n");
}
/* OUTPUT
   Start main
   In second if block
   End main*/

