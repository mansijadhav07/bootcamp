#include<stdio.h>
void main(){
        int x;
        printf("Enter value\n");
        scanf("%d",&x);

        switch(x){
                case 65:
                        printf("Value of x is 65\n");
                        break;
                case 66:
                        printf("Value of x is 66\n");
                        break;
                case 67:
                        printf("Value of x is 67\n");
                        break;
                case 68:
                        printf("Value of x is 68\n");
                        break;
                default:
                        printf("WRONG INPUT\n");
                        break;
        }
}
/* OUTPUT
   Enter value
   65
   Value of x is 65

   OUTPUT
   Enter value
   A
   WRONG INPUT
   (even if A has ASCII value 65 it will print WRONG INPUT)
*/
 
